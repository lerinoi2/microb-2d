//
//  OpenCVWrapper.h
//  FrameExtraction
//
//  Created by Кладов  Владимир on 17.05.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject
+ (NSString *)openCVVersionString;

+ (UIImage *) makeGrayFromImage:(UIImage *)image temp:(UIImage *)temp;

+ (UIImage *) makeContoursFromImage:(UIImage *)image step:(int)step type:(int)type settings:(NSDictionary*)settings;

+ (UIImage *) processImage:(UIImage *)image contour:(NSArray *)contour;

+ (UIImage *) showMeWhatYouGot:(UIImage *)image;

+ (NSArray *) tempContourKeyPoints:(NSArray *)contour;

+ (NSArray *) getFingersPositionNew:(UIImage *)image;

+ (NSArray *) getCurrentKeyPoints:(UIImage *)image;

+ (NSArray *) getFingersPosition:(UIImage *)image;

+ (NSArray *) getFingersPosition:(UIImage *)image approx:(int)approx;

+ (NSArray *) getContoursFromImage:(UIImage *)image;
@end
