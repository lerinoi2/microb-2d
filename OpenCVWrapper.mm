//
//  OpenCVWrapper.m
//  FrameExtraction
//
//  Created by Кладов  Владимир on 17.05.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

#import "OpenCVWrapper.h"
#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>
#import <opencv2/core/operations.hpp>

#import <opencv2/features2d/features2d.hpp>

float lineLength(cv::Point pointA, cv::Point pointB)
{
    return sqrt(pow(pointA.x - pointB.x, 2) + pow(pointA.y - pointB.y, 2));
}

bool wayToSort(cv::Vec2i a, cv::Vec2i b) { return a[0] < b[0]; }
bool wayToSortDistances(cv::Vec2f a, cv::Vec2f b) { return a[1] > b[1]; }

cv::Point pointBetween(cv::Point pointA, cv::Point pointB)
{
    float y = (pointA.y + pointB.y)/2;
    float x = (pointA.x + pointB.x)/2;
    return cv::Point(x, y);
}

bool isPointOnLine(cv::Point point, cv::Point pointA, cv::Point pointB)
{
    float x1 = (point.x - pointA.x);
    float x2 = (pointB.x - pointA.x);
    float dX = x1/x2;
    float y1 = (point.y - pointA.y);
    float y2 = (pointB.y - pointA.y);
    float dY = y1/y2;
    return fabs(dY - dX) < 0.01;
}

std::vector<cv::Point> findContour(cv::Mat binImage, int approx)
{
    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(binImage, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
    
    int largest_contour_index = 0;
    int largest_area = 0;
    if(contours.size() > 0){
        for( int i = 0; i < contours.size(); i++ ) // iterate through each contour.
        {
            double area = contourArea( contours[i] );  //  Find the area of contour
            
            if( area > largest_area )
            {
                largest_area = area;
                largest_contour_index = i;
            }
        }
        if(approx > 0){
            std::vector<cv::Point> approxContour;
            cv::approxPolyDP(contours[largest_contour_index], approxContour, approx, true);
            
            return approxContour;
        }else{
            return contours[largest_contour_index];
        }
    }else{
        return std::vector<cv::Point>(0);
    }
}

std::vector<int> hullsMergedIndexes(std::vector<cv::Point> contour, std::vector<int>hullIndexes)
{
    cv::Point point;
    std::vector<cv::Point> hullPoints(hullIndexes.size());
    for(int j = 0; j < hullIndexes.size(); j++){
        point = contour[hullIndexes[j]];
        hullPoints[j] = point;
    }
    std::vector<int> labels;
    struct Dist {
        bool operator()(const cv::Point &a, const cv::Point &b) {
            return std::abs(a.x - b.x) < 15 && std::abs(a.y - b.y) < 15;
        }
    };
    cv::partition(hullPoints, labels, Dist());
    int clNum = 0;
    for(int b = 0; b < labels.size(); b++){
        if(clNum < labels[b] + 1){
            clNum = labels[b] + 1;
        }
    }
    std::vector<std::vector<cv::Point>> clasters(clNum);
    std::vector<std::vector<int>> pointsIndexes(clNum);
    
    int pointIndex;
    for(int a = 0; a < labels.size(); a++){
        pointIndex = hullIndexes[a];
        point = contour[pointIndex];
        clasters[labels[a]].push_back(point);
        pointsIndexes[labels[a]].push_back(pointIndex);
    }
    
    
    // Finding centers of clasters
    int totalX = 0, totalY = 0, minDist = -1, minDistPointIndex = 0;
    float dist = 0;
    std::vector<cv::Point> centers(clasters.size());
    std::vector<int> result;
    for(int c = 0; c < clasters.size(); c++){
        totalX = 0;
        totalY = 0;
        for(int d = 0; d < clasters[c].size(); d++) {
            totalX += clasters[c][d].x;
            totalY += clasters[c][d].y;
        }
        
        centers[c] = cv::Point(totalX/clasters[c].size(), totalY/clasters[c].size());
        
        minDist = -1;
        minDistPointIndex = 0;
        for(int d = 0; d < clasters[c].size(); d++) {
            dist = lineLength(clasters[c][d], centers[c]);
            if(minDist < 0 || minDist < dist){
                minDist = dist;
                minDistPointIndex = d;
            }
        }
        result.push_back(pointsIndexes[c][minDistPointIndex]);
    }
    
    return result;
}

std::vector<int> filterDefects(std::vector<cv::Vec4i> defects){
    std::vector<int> indexes;
    for(int i = 0; i < defects.size(); i++) {
        //if(defects[i][3]/256.0 > 10){
            indexes.push_back(defects[i][2]);
        //}
    }
    return indexes;
}

int GetAngleABC( cv::Point a, cv::Point b, cv::Point c )
{
    std::vector<float> ab = { static_cast<float>(b.x - a.x), static_cast<float>(b.y - a.y) };
    std::vector<float> cb = { static_cast<float>(b.x - c.x), static_cast<float>(b.y - c.y) };
    
    // dot product
    float dot = (ab[0] * cb[0] + ab[1] * cb[1]);
    
    // length square of both vectors
    float abSqr = ab[0]* ab[0] + ab[1] * ab[1];
    float cbSqr = cb[0] * cb[0] + cb[1] * cb[1];
    
    // square of cosine of the needed angle
    float cosSqr = dot * dot / abSqr / cbSqr;
    
    // this is a known trigonometric equality:
    // cos(alpha * 2) = [ cos(alpha) ]^2 * 2 - 1
    float cos2 = 2 * cosSqr - 1;
    
    // Here's the only invocation of the heavy function.
    // It's a good idea to check explicitly if cos2 is within [-1 .. 1] range
    
    const float pi = 3.141592f;
    
    float alpha2 =
    (cos2 <= -1) ? pi :
    (cos2 >= 1) ? 0 :
    acosf(cos2);
    
    float rslt = alpha2 / 2;
    
    float rs = rslt * 180. / pi;
    
    
    // Now revolve the ambiguities.
    // 1. If dot product of two vectors is negative - the angle is definitely
    // above 90 degrees. Still we have no information regarding the sign of the angle.
    
    // NOTE: This ambiguity is the consequence of our method: calculating the cosine
    // of the double angle. This allows us to get rid of calling sqrt.
    
    if (dot < 0)
        rs = 180 - rs;
    
    // 2. Determine the sign. For this we'll use the Determinant of two vectors.
    
    float det = (ab[0] * cb[1] - ab[1] * cb[1]);
    if (det < 0)
        rs = -rs;
    
    return (int) floor(rs + 0.5);
}

std::vector<std::vector<cv::Point>> baseLine(std::vector<cv::Point> contour, std::vector<cv::Vec2i> keypoints, std::vector<cv::Vec4i> defects)
{
    std::vector<cv::Point> result;
    //Step 1:
    //Find contour's line on hull
    std::vector<std::vector<cv::Point>> result1;
    int next = 0, prev = 0, size = (int)keypoints.size();
    for(int i = 0; i < size; i++){
        if(keypoints[i][1] == 1){
            next = i == (size - 1) ? 0 : (i + 1);
            prev = i == 0 ? (size - 1) : (i - 1);
            
            if(keypoints[i][0] + 1 == keypoints[next][0] && keypoints[next][1] == 1){
                result1.push_back({contour[keypoints[i][0]], contour[keypoints[next][0]]});
            }
        }
    }
    
    //Step 2:
    //Find smallest defect
    int min = -1;
    for(int i = 0; i < defects.size(); i++){
        if(min < 0 || min > defects[i][3]){
            min = defects[i][3];
        }
    }
    
    //Find horizontal line on hull
    for(int i = 0; i < result1.size(); i++){
        if(keypoints[i][1] == 1){
            
        }
    }
    
    return result1;
}

int getNext(int index, int size){
    return index == (size - 1) ? 0 : index + 1;
}

int getPrev(int index, int size){
    return index == 0 ? (size - 1) : index - 1;
}

float getDistance(cv::Point A, cv::Point B, cv::Point C)
{
    return abs((B.y - A.y)*C.x - (B.x - A.x)*C.y + B.x*A.y - B.y*A.x)/sqrt(pow((B.y - A.y), 2) + pow((B.x - A.x), 2));
}

std::vector<int> getIndexesBetween(int A, int B, int size)
{
    std::vector<int> result;
    int next = getNext(A, size);
    while(next != B){
        result.push_back(next);
        next = getNext(next, size);
    }
    return result;
}

cv::Point getCentralPoint(std::vector<cv::Point> contour, std::vector<int> indexes)
{
    float summX = 0, summY = 0;
    int size = (int)indexes.size();
    for(int i = 0; i < size; i++){
        summX += contour[indexes[i]].x;
        summY += contour[indexes[i]].y;
    }
    return cv::Point(summX/size, summY/size);
}
cv::Point getCentralPoint(std::vector<cv::Point> contour)
{
    float summX = 0, summY = 0;
    int size = (int)contour.size();
    for(int i = 0; i < size; i++){
        summX += contour[i].x;
        summY += contour[i].y;
    }
    return cv::Point(summX/size, summY/size);
}

std::vector<cv::Point> getContourFromImage(cv::Mat imageMat, float approx)
{
    cv::Mat BIN;
    cv::Mat YCrCb;
    cv::cvtColor(imageMat, YCrCb, cv::COLOR_RGB2YCrCb);
    cv::Scalar min = cv::Scalar(0, 136, 77);
    cv::Scalar max = cv::Scalar(255, 173, 127);
    cv::inRange(YCrCb, min, max, YCrCb);
    cv::blur(YCrCb, YCrCb, cv::Size(5, 5));
    cv::threshold(YCrCb, BIN, 200, 255, cv::THRESH_BINARY);
    
    return findContour(BIN, approx);
}
std::vector<int> fromA2B(int indexA, int indexB, int size)
{
    std::vector<int> res;
    int nextIndex = getNext(indexA, size);
    while( nextIndex != indexB ) {
        res.push_back(nextIndex);
        nextIndex = getNext(nextIndex, size);
    }
    return res;
}

std::vector<int> indexes(int indexA, int indexB, int size)
{
    std::vector<int> A2B = fromA2B(indexA, indexB, size);
    std::vector<int> B2A = fromA2B(indexB, indexA, size);
    
    return A2B.size() < B2A.size() ? A2B : B2A;
}


@implementation OpenCVWrapper
+ (NSString *)openCVVersionString {
    return [NSString stringWithFormat:@"OpenCV Version %s",  CV_VERSION];
}

+(NSArray *) getFingersPosition:(UIImage *)image
{
    return [self getFingersPosition:(image) approx:(4)];
}

/*+(UIImage *) showMeWhatYouGot:(UIImage *)image
{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    
    std::vector<cv::Point> cont = getContourFromImage(imageMat, 2);
    if(cont.size() > 0){
        
        //
//        cv::polylines(imageMat, cont, true, cv::Scalar(0, 0, 255, 100), 1);
//        for(int i = 0; i < cont.size(); i++){
//            cv::circle(imageMat, cont[i], 2, cv::Scalar(0, 0, 255, 255), 2);
//        }
        //
        
        cv::Point center;
        double dist, maxdist = -1;
        for(int i = 0; i < imageMat.cols; i += 5)
        {
            for(int j = 0; j < imageMat.rows; j += 5)
            {
                dist = cv::pointPolygonTest(cont, cv::Point(i,j), true);
                if(dist > maxdist)
                {
                    maxdist = dist;
                    center = cv::Point(i,j);
                }
            }
        }
        
        if(maxdist > 0){
            //
            cv::circle(imageMat, center, 2, cv::Scalar(255, 0, 0, 255), 1);
            cv::circle(imageMat, center, maxdist, cv::Scalar(255, 0, 0, 255), 1);
            //
        }
        
        std::vector<float> distancesToCenter;
        for(int i = 0; i < cont.size(); i++){
            distancesToCenter.push_back(lineLength(center, cont[i]));
        }
        std::vector<int> localMinimums;
        for(int i = 0; i < cont.size(); i++){
            int next = getNext(i, (int)cont.size());
            int prev = getPrev(i, (int)cont.size());
            if(distancesToCenter[next] > distancesToCenter[i] && distancesToCenter[prev] > distancesToCenter[i]){
                cv::circle(imageMat, cont[i], 3, cv::Scalar(255, 0, 100, 255), 3);
                localMinimums.push_back(i);
            }
        }
        std::vector<cv::Vec2i> keyPoints;
        for(int j = 0; j < localMinimums.size(); j++){
            int next = getNext(j, (int)localMinimums.size());
            std::vector<int> line = indexes(localMinimums[j], localMinimums[next], (int)cont.size());
            //cv::Point c = pointBetween(cont[j], cont[next]);
            
            float max = 0;
            int maxIndex = localMinimums[j];
            for(int i = 0; i < line.size(); i++){
                float length = lineLength(cont[line[i]], center);
                
                if(length > max){
                    max = length;
                    maxIndex = line[i];
                }
            }
            //
            cv::circle(imageMat, cont[maxIndex], 3, cv::Scalar(100, 0, 255, 255), 3);
            //
            
            keyPoints.push_back(cv::Vec2i(localMinimums[j], 0));
            keyPoints.push_back(cv::Vec2i(maxIndex, 1));
        }
        
        std::vector<cv::Vec3i> fingers;
        float minSum = 0;
        for(int i = 0; i < keyPoints.size(); i++){
            int next = getNext(i, (int)keyPoints.size());
            int prev = getPrev(i, (int)keyPoints.size());
            float angle;
            if(keyPoints[i][1] == 0){
                minSum += lineLength(cont[keyPoints[i][0]], center);
            }
            if(
               keyPoints[i][1] == 1 &&
               keyPoints[next][1] == 0 &&
               keyPoints[prev][1] == 0
               ){
                
                angle = GetAngleABC(cont[keyPoints[prev][0]], cont[keyPoints[i][0]], cont[keyPoints[next][0]]);
                
                if(fabs(angle) < 35){
                    fingers.push_back(cv::Vec3i(keyPoints[i][0], keyPoints[prev][0], keyPoints[next][0]));
                    cv::circle(imageMat, cont[keyPoints[i][0]], 6, cv::Scalar(255, 255, 255, 255), 6);
                }
            }
        }
        cv::putText(imageMat, std::to_string(fingers.size()), cv::Point(imageMat.cols/2, 50), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0, 0, 0, 255));
        float p = minSum/localMinimums.size()/(maxdist);
        cv::putText(imageMat, std::to_string(p), cv::Point(imageMat.cols/2, imageMat.rows/2 - 50), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(255, 255, 0, 255));
        if(fingers.size() == 5 && p < 1.5){
            //
            cv::putText(imageMat, "YES", cv::Point(imageMat.cols/2, imageMat.rows/2), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(255, 255, 0, 255));
            //
            float maxAngle = 0;
            float angle;
            int startIndex = 0;
            for(int i = 0; i < fingers.size(); i++){
                angle = fabs(GetAngleABC(cont[fingers[i][0]], center, cont[fingers[getNext(i, (int)fingers.size())][0]]));
                if(angle > maxAngle){
                    maxAngle = angle;
                    startIndex = i + 1;
                }
            }
            std::vector<cv::Vec3i> newFingers(fingers.size());
            if((startIndex)%fingers.size() != 0){
                for(int i = 0; i < fingers.size(); i++){
                    newFingers[i] = fingers[(i + startIndex)%(fingers.size())];
                }
            }
            fingers = newFingers;
            
            float leftAngle = GetAngleABC(cont[fingers[0][0]], center, cont[fingers[1][0]]);
            float rightAngle = GetAngleABC(cont[fingers[3][0]], center, cont[fingers[4][0]]);
            
            int F1, F2, F3, F4, F5, d0, d12, d23, d34, d45, d5;
            
            if(fabs(leftAngle) > fabs(rightAngle)){
                d0 = fingers[0][1];
                F1 = fingers[0][0];
                d12 = fingers[0][2];
                F2 = fingers[1][0];
                d23 = fingers[1][2];
                F3 = fingers[2][0];
                d34 = fingers[2][2];
                F4 = fingers[3][0];
                d45 = fingers[3][2];
                F5 = fingers[4][0];
                d5 = fingers[4][2];
            }else{
                d0 = fingers[4][2];
                F1 = fingers[4][0];
                d12 = fingers[4][1];
                F2 = fingers[3][0];
                d23 = fingers[3][1];
                F3 = fingers[2][0];
                d34 = fingers[2][1];
                F4 = fingers[1][0];
                d45 = fingers[1][1];
                F5 = fingers[0][0];
                d5 = fingers[0][1];
            }
        }
    }
    return MatToUIImage(imageMat);
}*/

+(UIImage *) showMeWhatYouGot:(UIImage *)image
{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    
    std::vector<cv::Point> cont = getContourFromImage(imageMat, 2);
    if(cont.size() > 0){
        
        cv::Point center;
        double dist, maxdist = -1;
        for(int i = 0; i < imageMat.cols; i += 5)
        {
            for(int j = 0; j < imageMat.rows; j += 5)
            {
                dist = cv::pointPolygonTest(cont, cv::Point(i,j), true);
                if(dist > maxdist)
                {
                    maxdist = dist;
                    center = cv::Point(i,j);
                }
            }
        }
        
        if(maxdist > 0){
            //*
            cv::circle(imageMat, center, 2, cv::Scalar(255, 0, 0, 255), 1);
            cv::circle(imageMat, center, maxdist, cv::Scalar(255, 0, 0, 255), 1);
            //*
        }
        
        std::vector<float> distancesToCenter;
        for(int i = 0; i < cont.size(); i++){
            distancesToCenter.push_back(lineLength(center, cont[i]));
        }
        std::vector<int> localMinimums;
        for(int i = 0; i < cont.size(); i++){
            int next = getNext(i, (int)cont.size());
            int prev = getPrev(i, (int)cont.size());
            if(distancesToCenter[next] > distancesToCenter[i] && distancesToCenter[prev] > distancesToCenter[i]){
                localMinimums.push_back(i);
            }
        }
        std::vector<cv::Vec2i> keyPoints;
        for(int j = 0; j < localMinimums.size(); j++){
            int next = getNext(j, (int)localMinimums.size());
            std::vector<int> line = indexes(localMinimums[j], localMinimums[next], (int)cont.size());
            
            float max = 0;
            int maxIndex = localMinimums[j];
            for(int i = 0; i < line.size(); i++){
                float length = lineLength(cont[line[i]], center);
                
                if(length > max){
                    max = length;
                    maxIndex = line[i];
                }
            }
            
            keyPoints.push_back(cv::Vec2i(localMinimums[j], 0));
            keyPoints.push_back(cv::Vec2i(maxIndex, 1));
        }
        
        std::vector<cv::Vec3i> fingers;
        float minSum = 0;
        for(int i = 0; i < keyPoints.size(); i++){
            int next = getNext(i, (int)keyPoints.size());
            int prev = getPrev(i, (int)keyPoints.size());
            float angle;
            if(keyPoints[i][1] == 0){
                minSum += lineLength(cont[keyPoints[i][0]], center);
            }
            if(
               keyPoints[i][1] == 1 &&
               keyPoints[next][1] == 0 &&
               keyPoints[prev][1] == 0
               ){
                
                angle = GetAngleABC(cont[keyPoints[prev][0]], cont[keyPoints[i][0]], cont[keyPoints[next][0]]);
                
                if(fabs(angle) < 35){
                    fingers.push_back(cv::Vec3i(keyPoints[i][0], keyPoints[prev][0], keyPoints[next][0]));
                    cv::circle(imageMat, cont[keyPoints[i][0]], 6, cv::Scalar(255, 255, 255, 255), 6);
                }
            }
        }
        float p = minSum/localMinimums.size()/(maxdist);
        if(fingers.size() == 5 && p < 1.5){
            cv::putText(imageMat, "YES", cv::Point(imageMat.cols/2, imageMat.rows/2), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(255, 255, 0, 255));
            float maxAngle = 0;
            float angle;
            int startIndex = 0;
            for(int i = 0; i < fingers.size(); i++){
                angle = fabs(GetAngleABC(cont[fingers[i][0]], center, cont[fingers[getNext(i, (int)fingers.size())][0]]));
                if(angle > maxAngle){
                    maxAngle = angle;
                    startIndex = i + 1;
                }
            }
            std::vector<cv::Vec3i> newFingers(fingers.size());
            if((startIndex)%fingers.size() != 0){
                for(int i = 0; i < fingers.size(); i++){
                    newFingers[i] = fingers[(i + startIndex)%(fingers.size())];
                }
            }
            fingers = newFingers;
            
            float leftAngle = GetAngleABC(cont[fingers[0][0]], center, cont[fingers[1][0]]);
            float rightAngle = GetAngleABC(cont[fingers[3][0]], center, cont[fingers[4][0]]);
            
            int F1, F2, F3, F4, F5, d0, d12, d23, d34, d45, d5;
            
            if(fabs(leftAngle) > fabs(rightAngle)){
                d0 = fingers[0][1];
                F1 = fingers[0][0];
                d12 = fingers[0][2];
                F2 = fingers[1][0];
                d23 = fingers[1][2];
                F3 = fingers[2][0];
                d34 = fingers[2][2];
                F4 = fingers[3][0];
                d45 = fingers[3][2];
                F5 = fingers[4][0];
                d5 = fingers[4][2];
            }else{
                d0 = fingers[4][2];
                F1 = fingers[4][0];
                d12 = fingers[4][1];
                F2 = fingers[3][0];
                d23 = fingers[3][1];
                F3 = fingers[2][0];
                d34 = fingers[2][1];
                F4 = fingers[1][0];
                d45 = fingers[1][1];
                F5 = fingers[0][0];
                d5 = fingers[0][1];
            }
        }
    }
    return MatToUIImage(imageMat);
}

+(NSArray *) getFingersPositionNew:(UIImage *)image
{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    
    std::vector<cv::Point> cont = getContourFromImage(imageMat, 2);
    if(cont.size() > 0){
        
        cv::Point center;
        double dist, maxdist = -1;
        for(int i = 0; i < imageMat.cols; i += 5)
        {
            for(int j = 0; j < imageMat.rows; j += 5)
            {
                dist = cv::pointPolygonTest(cont, cv::Point(i,j), true);
                if(dist > maxdist)
                {
                    maxdist = dist;
                    center = cv::Point(i,j);
                }
            }
        }
        
        std::vector<float> distancesToCenter;
        for(int i = 0; i < cont.size(); i++){
            distancesToCenter.push_back(lineLength(center, cont[i]));
        }
        std::vector<int> localMinimums;
        for(int i = 0; i < cont.size(); i++){
            int next = getNext(i, (int)cont.size());
            int prev = getPrev(i, (int)cont.size());
            if(distancesToCenter[next] > distancesToCenter[i] && distancesToCenter[prev] > distancesToCenter[i]){
                localMinimums.push_back(i);
            }
        }
        std::vector<cv::Vec2i> keyPoints;
        for(int j = 0; j < localMinimums.size(); j++){
            int next = getNext(j, (int)localMinimums.size());
            std::vector<int> line = indexes(localMinimums[j], localMinimums[next], (int)cont.size());
            
            float max = 0;
            int maxIndex = localMinimums[j];
            for(int i = 0; i < line.size(); i++){
                float length = lineLength(cont[line[i]], center);
                
                if(length > max){
                    max = length;
                    maxIndex = line[i];
                }
            }
            
            keyPoints.push_back(cv::Vec2i(localMinimums[j], 0));
            keyPoints.push_back(cv::Vec2i(maxIndex, 1));
        }
        
        std::vector<cv::Vec3i> fingers;
        float minSum = 0;
        for(int i = 0; i < keyPoints.size(); i++){
            int next = getNext(i, (int)keyPoints.size());
            int prev = getPrev(i, (int)keyPoints.size());
            float angle;
            if(keyPoints[i][1] == 0){
                minSum += lineLength(cont[keyPoints[i][0]], center);
            }
            if(
               keyPoints[i][1] == 1 &&
               keyPoints[next][1] == 0 &&
               keyPoints[prev][1] == 0
               ){
                
                angle = GetAngleABC(cont[keyPoints[prev][0]], cont[keyPoints[i][0]], cont[keyPoints[next][0]]);
                
                if(fabs(angle) < 35){
                    fingers.push_back(cv::Vec3i(keyPoints[i][0], keyPoints[prev][0], keyPoints[next][0]));
                }
            }
        }
        float p = minSum/localMinimums.size()/(maxdist);
        if(fingers.size() == 5 && p < 1.5){
            float maxAngle = 0;
            float angle;
            int startIndex = 0;
            for(int i = 0; i < fingers.size(); i++){
                angle = fabs(GetAngleABC(cont[fingers[i][0]], center, cont[fingers[getNext(i, (int)fingers.size())][0]]));
                if(angle > maxAngle){
                    maxAngle = angle;
                    startIndex = i + 1;
                }
            }
            std::vector<cv::Vec3i> newFingers(fingers.size());
            if((startIndex)%fingers.size() != 0){
                for(int i = 0; i < fingers.size(); i++){
                    newFingers[i] = fingers[(i + startIndex)%(fingers.size())];
                }
            }
            fingers = newFingers;
            
            float leftAngle = GetAngleABC(cont[fingers[0][0]], center, cont[fingers[1][0]]);
            float rightAngle = GetAngleABC(cont[fingers[3][0]], center, cont[fingers[4][0]]);
            
            int F1, F2, F3, F4, F5, d0, d12, d23, d34, d45, d5;
            
            if(fabs(leftAngle) > fabs(rightAngle)){
                d0 = fingers[0][1];
                F1 = fingers[0][0];
                d12 = fingers[0][2];
                F2 = fingers[1][0];
                d23 = fingers[1][2];
                F3 = fingers[2][0];
                d34 = fingers[2][2];
                F4 = fingers[3][0];
                d45 = fingers[3][2];
                F5 = fingers[4][0];
                d5 = fingers[4][2];
            }else{
                d0 = fingers[4][2];
                F1 = fingers[4][0];
                d12 = fingers[4][1];
                F2 = fingers[3][0];
                d23 = fingers[3][1];
                F3 = fingers[2][0];
                d34 = fingers[2][1];
                F4 = fingers[1][0];
                d45 = fingers[1][1];
                F5 = fingers[0][0];
                d5 = fingers[0][1];
            }
            NSMutableArray *points = [NSMutableArray array];
            cv::Point point;
            [points addObject:[NSNumber numberWithInt:(center.x)]];
            [points addObject:[NSNumber numberWithInt:(center.y)]];
            
            [points addObject:[NSNumber numberWithInt:(F1)]];
            
            [points addObject:[NSNumber numberWithInt:(F2)]];
            
            [points addObject:[NSNumber numberWithInt:(F3)]];
            
            [points addObject:[NSNumber numberWithInt:(F4)]];
            
            [points addObject:[NSNumber numberWithInt:(F5)]];
            
            [points addObject:[NSNumber numberWithInt:(d0)]];
            
            [points addObject:[NSNumber numberWithInt:(d12)]];
            
            [points addObject:[NSNumber numberWithInt:(d23)]];
            
            [points addObject:[NSNumber numberWithInt:(d34)]];
            
            [points addObject:[NSNumber numberWithInt:(d45)]];
            
            [points addObject:[NSNumber numberWithInt:(d5)]];
            
            for(int i = 0; i < cont.size(); i++){
                [points addObject:[NSNumber numberWithInt:(cont[i].x)]];
                [points addObject:[NSNumber numberWithInt:(cont[i].y)]];
            }
            
            return points;
        }
    }
    NSMutableArray *points = [NSMutableArray array];
    return points;
}


+(NSArray *) getFingersPosition:(UIImage *)image approx:(int)approx
{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    
    std::vector<cv::Point> cont = getContourFromImage(imageMat, approx);
    if(cont.size() > 0){
        std::vector<int> hullsIndexes;
        std::vector<int> hullIndices;
        std::vector<cv::Vec4i> defects;
        std::vector<int> defectsIndexes;
        cv::convexHull(cv::Mat(cont), hullIndices);
        hullsIndexes = hullsMergedIndexes(cont, hullIndices);
        cv::convexityDefects(cont, hullsIndexes, defects);
        defectsIndexes = filterDefects(defects);
        
        cv::Point center;
        double dist, maxdist = -1;
        for(int i = 0; i < imageMat.cols; i += 5)
        {
            for(int j = 0; j < imageMat.rows; j += 5)
            {
                dist = cv::pointPolygonTest(cont, cv::Point(i,j), true);
                if(dist > maxdist)
                {
                    maxdist = dist;
                    center = cv::Point(i,j);
                }
            }
        }
        
        if(maxdist > 0){
            cv::Point point;
            for(int i = 0; i < hullsIndexes.size(); i++) {
                point = cont[hullsIndexes[i]];
            }
        }
        
        std::vector<cv::Vec2i> keyPoints;
        for(int j = 0; j < hullsIndexes.size(); j++){
            keyPoints.push_back(cv::Vec2i(hullsIndexes[j], 1));
        }
        for(int j = 0; j < defectsIndexes.size(); j++){
            keyPoints.push_back(cv::Vec2i(defectsIndexes[j], 0));
        }
        std::sort(keyPoints.begin(), keyPoints.end(), wayToSort);
        
        std::vector<cv::Vec3i> fingers;
        for(int i = 0; i < keyPoints.size(); i++){
            int next = getNext(i, (int)keyPoints.size());
            int prev = getPrev(i, (int)keyPoints.size());
            float angle;
            if(
               keyPoints[i][1] == 1 &&
               keyPoints[next][1] == 0 &&
               keyPoints[prev][1] == 0
               ){
                
                angle = GetAngleABC(cont[keyPoints[prev][0]], cont[keyPoints[i][0]], cont[keyPoints[next][0]]);
                if(fabs(angle) < 30){
                    fingers.push_back(cv::Vec3i(keyPoints[i][0], keyPoints[prev][0], keyPoints[next][0]));
                }
                
            }
        }
        if(fingers.size() == 5){
            float maxAngle = 0;
            float angle;
            int startIndex = 0;
            for(int i = 0; i < fingers.size(); i++){
                angle = fabs(GetAngleABC(cont[fingers[i][0]], center, cont[fingers[getNext(i, (int)fingers.size())][0]]));
                if(angle > maxAngle){
                    maxAngle = angle;
                    startIndex = i + 1;
                }
            }
            std::vector<cv::Vec3i> newFingers(fingers.size());
            if((startIndex)%fingers.size() != 0){
                for(int i = 0; i < fingers.size(); i++){
                    newFingers[i] = fingers[(i + startIndex)%(fingers.size())];
                }
            }
            fingers = newFingers;
            
            float leftAngle = GetAngleABC(cont[fingers[0][0]], center, cont[fingers[1][0]]);
            float rightAngle = GetAngleABC(cont[fingers[3][0]], center, cont[fingers[4][0]]);
            
            int F1, F2, F3, F4, F5, d0, d12, d23, d34, d45, d5;
            
            if(fabs(leftAngle) > fabs(rightAngle)){
                d0 = fingers[0][1];
                F1 = fingers[0][0];
                d12 = fingers[0][2];
                F2 = fingers[1][0];
                d23 = fingers[1][2];
                F3 = fingers[2][0];
                d34 = fingers[2][2];
                F4 = fingers[3][0];
                d45 = fingers[3][2];
                F5 = fingers[4][0];
                d5 = fingers[4][2];
            }else{
                d0 = fingers[4][2];
                F1 = fingers[4][0];
                d12 = fingers[4][1];
                F2 = fingers[3][0];
                d23 = fingers[3][1];
                F3 = fingers[2][0];
                d34 = fingers[2][1];
                F4 = fingers[1][0];
                d45 = fingers[1][1];
                F5 = fingers[0][0];
                d5 = fingers[0][1];
            }
            
            NSMutableArray *points = [NSMutableArray array];
            cv::Point point;
            [points addObject:[NSNumber numberWithInt:(center.x)]];
            [points addObject:[NSNumber numberWithInt:(center.y)]];
            
            [points addObject:[NSNumber numberWithInt:(F1)]];
            
            [points addObject:[NSNumber numberWithInt:(F2)]];
            
            [points addObject:[NSNumber numberWithInt:(F3)]];
            
            [points addObject:[NSNumber numberWithInt:(F4)]];
            
            [points addObject:[NSNumber numberWithInt:(F5)]];
            
            [points addObject:[NSNumber numberWithInt:(d0)]];
            
            [points addObject:[NSNumber numberWithInt:(d12)]];
            
            [points addObject:[NSNumber numberWithInt:(d23)]];
            
            [points addObject:[NSNumber numberWithInt:(d34)]];
            
            [points addObject:[NSNumber numberWithInt:(d45)]];
            
            [points addObject:[NSNumber numberWithInt:(d5)]];
            
            for(int i = 0; i < cont.size(); i++){
                [points addObject:[NSNumber numberWithInt:(cont[i].x)]];
                [points addObject:[NSNumber numberWithInt:(cont[i].y)]];
            }
            
            return points;
        }
    }
    NSMutableArray *points = [NSMutableArray array];
    return points;
}

+(NSArray *) getCurrentKeyPoints:(UIImage *)image
{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    std::vector<cv::Point> cont = getContourFromImage(imageMat, 4);
    if(cont.size() > 0){
        std::vector<int> hullsIndexes;
        std::vector<int> hullIndices;
        std::vector<int> defectsIndexes;
        cv::convexHull(cv::Mat(cont), hullIndices);
        hullsIndexes = hullsMergedIndexes(cont, hullIndices);
        
        cv::Point center;
        double dist, maxdist = -1;
        for(int i = 0; i < imageMat.cols; i += 5)
        {
            for(int j = 0; j < imageMat.rows; j += 5)
            {
                dist = cv::pointPolygonTest(cont, cv::Point(i,j), true);
                if(dist > maxdist)
                {
                    maxdist = dist;
                    center = cv::Point(i,j);
                }
            }
        }
        
        if(maxdist > 0){
            NSMutableArray *points = [NSMutableArray array];
            cv::Point point;
            [points addObject:[NSNumber numberWithInt:(center.x)]];
            [points addObject:[NSNumber numberWithInt:(center.y)]];
            for(int i = 0; i < hullsIndexes.size(); i++) {
                point = cont[hullsIndexes[i]];
                [points addObject:[NSNumber numberWithInt:(point.x)]];
                [points addObject:[NSNumber numberWithInt:(point.y)]];
            }
            return points;
        }
    }
    
    NSMutableArray *points = [NSMutableArray array];
    return points;
}

+(NSArray *) tempContourKeyPoints:(NSArray *)contour
{
    std::vector<cv::Point> tempContour;
    for (int i = 0; i < [contour count]; i+=2) {
        int x = [((NSNumber*)[contour objectAtIndex:i]) intValue];
        int y = [((NSNumber*)[contour objectAtIndex:(i+1)]) intValue];
        tempContour.push_back(cv::Point(x, y));
    }
    std::vector<int> hullIndices;
    std::vector<int> hullsIndexes;
    cv::convexHull(cv::Mat(tempContour), hullIndices);
    hullsIndexes = hullsMergedIndexes(tempContour, hullIndices);
    
    cv::Point center;
    double dist, maxdist = -1;
    for(int i = 0; i < 320; i += 5)
    {
        for(int j = 0; j < 480; j += 5)
        {
            dist = cv::pointPolygonTest(tempContour, cv::Point(i,j), true);
            if(dist > maxdist)
            {
                maxdist = dist;
                center = cv::Point(i,j);
            }
        }
    }
    
    if(maxdist > 0){
        if(hullsIndexes.size() > 5){
            std::vector<cv::Vec2f> hullsDistancesFromCenter;
            for(int i = 0; i < hullsIndexes.size(); i++){
                hullsDistancesFromCenter.push_back(cv::Vec2f(hullsIndexes[i], lineLength(tempContour[hullsIndexes[i]], center)));
            }
            std::sort(hullsDistancesFromCenter.begin(), hullsDistancesFromCenter.end(), wayToSortDistances);
            
            NSMutableArray *points = [NSMutableArray array];
            cv::Point point;
            [points addObject:[NSNumber numberWithInt:(center.x)]];
            [points addObject:[NSNumber numberWithInt:(center.y)]];
            for(int i = 0; i < 5; i++) {
                point = tempContour[hullsDistancesFromCenter[i][0]];
                [points addObject:[NSNumber numberWithInt:(point.x)]];
                [points addObject:[NSNumber numberWithInt:(point.y)]];
            }
            return points;
        }
    }
    
    NSMutableArray *points = [NSMutableArray array];
    return points;
}

+(UIImage *) processImage:()image contour:(NSArray *)contour
{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    /*
    std::vector<cv::Point> cont = getContourFromImage(imageMat, 4);
    if(cont.size() > 0){
        std::vector<int> hullsIndexes;
        std::vector<int> hullIndices;
        std::vector<cv::Vec4i> defects;
        std::vector<int> defectsIndexes;
        cv::convexHull(cv::Mat(cont), hullIndices);
        hullsIndexes = hullsMergedIndexes(cont, hullIndices);
        cv::convexityDefects(cont, hullsIndexes, defects);
        defectsIndexes = filterDefects(defects);
        
        for(int i = 0; i < defectsIndexes.size(); i++){
            cv::circle(imageMat, cont[defectsIndexes[i]], 5, cv::Scalar(255, 255, 0, 255), 2);
        }
        
        cv::Point center;
        double dist, maxdist = -1;
        for(int i = 0; i < imageMat.cols; i += 5)
        {
            for(int j = 0; j < imageMat.rows; j += 5)
            {
                dist = cv::pointPolygonTest(cont, cv::Point(i,j), true);
                if(dist > maxdist)
                {
                    maxdist = dist;
                    center = cv::Point(i,j);
                }
            }
        }
        
        if(maxdist > 0){
            cv::Point point;
            cv::circle(imageMat, center, 5, cv::Scalar(255, 0, 0, 255), 2);
            cv::circle(imageMat, center, maxdist, cv::Scalar(255, 0, 0, 255), 1);
            for(int i = 0; i < hullsIndexes.size(); i++) {
                point = cont[hullsIndexes[i]];
                cv::circle(imageMat, point, 5, cv::Scalar(255, 0, 0, 255), 2);
            }
        }
        
        std::vector<cv::Vec2i> keyPoints;
        for(int j = 0; j < hullsIndexes.size(); j++){
            keyPoints.push_back(cv::Vec2i(hullsIndexes[j], 1));
        }
        for(int j = 0; j < defectsIndexes.size(); j++){
            keyPoints.push_back(cv::Vec2i(defectsIndexes[j], 0));
        }
        std::sort(keyPoints.begin(), keyPoints.end(), wayToSort);
        
        std::vector<cv::Vec3i> fingers;
        for(int i = 0; i < keyPoints.size(); i++){
            int next = getNext(i, (int)keyPoints.size());
            int prev = getPrev(i, (int)keyPoints.size());
            float angle;
            if(
               keyPoints[i][1] == 1 &&
               keyPoints[next][1] == 0 &&
               keyPoints[prev][1] == 0
               ){
                
                angle = GetAngleABC(cont[keyPoints[prev][0]], cont[keyPoints[i][0]], cont[keyPoints[next][0]]);
                if(fabs(angle) < 30){
                    fingers.push_back(cv::Vec3i(keyPoints[i][0], keyPoints[prev][0], keyPoints[next][0]));
                    cv::circle(imageMat, cont[keyPoints[i][0]], 5, cv::Scalar(0, 255, 0, 255), 6);
                }
                
            }
            
            if(
               keyPoints[i][1] == 0 &&
               keyPoints[next][1] == 1 &&
               keyPoints[prev][1] == 1
               ){
                cv::circle(imageMat, cont[keyPoints[i][0]], 2, cv::Scalar(255, 255, 0, 255), 2);
            }
        }
        if(fingers.size() == 5){
            cv::putText(imageMat, "YES", cv::Point(imageMat.cols/2, imageMat.rows/2), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(255, 255, 0, 255));
            
            float maxAngle = 0;
            float angle;
            int startIndex = 0;
            for(int i = 0; i < fingers.size(); i++){
                angle = fabs(GetAngleABC(cont[fingers[i][0]], center, cont[fingers[getNext(i, (int)fingers.size())][0]]));
                if(angle > maxAngle){
                    maxAngle = angle;
                    startIndex = i + 1;
                }
                //cv::putText(imageMat, std::to_string(i), cont[fingers[i][0]], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0, 255));
            }
            cv::putText(imageMat, std::to_string(startIndex), cont[fingers[startIndex][0]], cv::FONT_HERSHEY_COMPLEX, 1, cv::Scalar(255, 0, 0, 255));
            std::vector<cv::Vec3i> newFingers(fingers.size());
            if((startIndex)%fingers.size() != 0){
                for(int i = 0; i < fingers.size(); i++){
                    newFingers[i] = fingers[(i + startIndex)%(fingers.size())];
                }
            }
            fingers = newFingers;
            
            float leftAngle = GetAngleABC(cont[fingers[0][0]], center, cont[fingers[1][0]]);
            float rightAngle = GetAngleABC(cont[fingers[3][0]], center, cont[fingers[4][0]]);
            
            int F1, F2, F3, F4, F5, d0, d12, d23, d34, d45, d5, d6;
            
            if(fabs(leftAngle) > fabs(rightAngle)){
                cv::putText(imageMat, "Left", cv::Point(50, 50), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 255,255, 255));
                d0 = fingers[0][1];
                F1 = fingers[0][0];
                d12 = fingers[0][2];
                F2 = fingers[1][0];
                d23 = fingers[1][2];
                F3 = fingers[2][0];
                d34 = fingers[2][2];
                F4 = fingers[3][0];
                d45 = fingers[3][2];
                F5 = fingers[4][0];
                d5 = fingers[4][2];
            }else{
                cv::putText(imageMat, "Right", cv::Point(50, 50), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 0, 255));
                d0 = fingers[4][2];
                F1 = fingers[4][0];
                d12 = fingers[4][1];
                F2 = fingers[3][0];
                d23 = fingers[3][1];
                F3 = fingers[2][0];
                d34 = fingers[2][1];
                F4 = fingers[1][0];
                d45 = fingers[1][1];
                F5 = fingers[0][0];
                d5 = fingers[0][1];
            }
            
            cv::putText(imageMat, "d0", cont[d0], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0, 255));
            cv::putText(imageMat, "F1", cont[F1], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 0, 255));
            cv::putText(imageMat, "d12", cont[d12], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0, 255));
            cv::putText(imageMat, "F2", cont[F2], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 0, 255));
            cv::putText(imageMat, "d23", cont[d23], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0, 255));
            cv::putText(imageMat, "F3", cont[F3], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 0, 255));
            cv::putText(imageMat, "d34", cont[d34], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0, 255));
            cv::putText(imageMat, "F4", cont[F4], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 0, 255));
            cv::putText(imageMat, "d45", cont[d45], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0, 255));
            cv::putText(imageMat, "F5", cont[F5], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 0, 255));
            cv::putText(imageMat, "d5", cont[d5], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0, 255));
            
        }
    }*/
    
    if([contour count] > 0){
        std::vector<cv::Point> tempContour;
        for (int i = 0; i < [contour count]; i+=2) {
            int x = [((NSNumber*)[contour objectAtIndex:i]) intValue];
            int y = [((NSNumber*)[contour objectAtIndex:(i+1)]) intValue];
            tempContour.push_back(cv::Point(x, y));
        }
        
        cv::polylines(imageMat, tempContour, true, cv::Scalar(255, 255, 255, 255), 3);
    }
    
    return MatToUIImage(imageMat);
}

+(NSArray *) getContoursFromImage:()image
{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    std::vector<cv::Point> contour = getContourFromImage(imageMat, 2);
    cv::polylines(imageMat, contour, true, cv::Scalar(255, 255, 255, 255), 3);
    
    NSMutableArray *points = [NSMutableArray array];
    for(int i = 0; i < contour.size(); i++) {
        [points addObject:[NSNumber numberWithInt:(contour[i].x)]];
        [points addObject:[NSNumber numberWithInt:(contour[i].y)]];
    }
    
    return points;
}

+(UIImage *) makeContoursFromImage:()image step:(int)step type:(int)type settings:(NSDictionary*)settings
{
    cv::Mat imageMat, BIN;
    UIImageToMat(image, imageMat);
    
    cv::Mat YCrCb;
    cv::cvtColor(imageMat, YCrCb, cv::COLOR_RGB2YCrCb);
    cv::Scalar min = cv::Scalar([settings[@"Y_min"] floatValue], [settings[@"Cr_min"] floatValue], [settings[@"Cb_min"] floatValue]);
    cv::Scalar max = cv::Scalar([settings[@"Y_max"] floatValue], [settings[@"Cr_max"] floatValue], [settings[@"Cb_max"] floatValue]);
    cv::inRange(YCrCb, min, max, YCrCb);
    cv::threshold(YCrCb, BIN, 200, 255, cv::THRESH_BINARY);
    

    std::vector<int> hullIndices;
    std::vector<cv::Point> contour = findContour(BIN, 4);
    std::vector<int> hullsIndexes;
    std::vector<cv::Vec4i> defects;
    std::vector<int> defectsIndexes;
    if(contour.size() > 0){
        cv::convexHull(cv::Mat(contour), hullIndices);
        cv::convexityDefects(contour, hullIndices, defects);
        defectsIndexes = filterDefects(defects);
        
        hullsIndexes = hullsMergedIndexes(contour, hullIndices);
        //hullsIndexes = hullIndices;
        
        cv::Point center;
        double dist, maxdist = -1;
        for(int i = 0; i < imageMat.cols; i += 5)
        {
            for(int j = 0; j < imageMat.rows; j += 5)
            {
                dist = cv::pointPolygonTest(contour, cv::Point(i,j), true);
                if(dist > maxdist)
                {
                    maxdist = dist;
                    center = cv::Point(i,j);
                }
            }
        }
        if(maxdist > 0){
            cv::circle(imageMat, center, maxdist, cv::Scalar(255, 0, 0, 255));
            cv::circle(imageMat, center, 3, cv::Scalar(255, 0, 0, 255), 4);
            
            for(int j = 0; j < hullsIndexes.size(); j++){
                cv::line(imageMat, center, contour[hullsIndexes[j]], cv::Scalar(255, 0, 0, 255));
                cv::circle(imageMat, contour[hullsIndexes[j]], 2, cv::Scalar(255, 0, 0, 255), 2);
            }
        }
        
        
        std::vector<cv::Vec2i> keyPoints;
        for(int j = 0; j < hullsIndexes.size(); j++){
            keyPoints.push_back(cv::Vec2i(hullsIndexes[j], 1));
        }
        for(int j = 0; j < defectsIndexes.size(); j++){
            keyPoints.push_back(cv::Vec2i(defectsIndexes[j], 0));
        }
        std::sort(keyPoints.begin(), keyPoints.end(), wayToSort);
        
        for(int i = 0; i < keyPoints.size(); i++){
            int next = getNext(i, (int)keyPoints.size());
            int prev = getPrev(i, (int)keyPoints.size());
            
//            float angle;
            if(
               keyPoints[i][1] == 1 &&
               keyPoints[next][1] == 0 &&
               keyPoints[prev][1] == 0
               ){
                cv::circle(imageMat, contour[keyPoints[i][0]], 2, cv::Scalar(255, 0, 255, 255), 2);
            }
            
            if(
               keyPoints[i][1] == 0 &&
               keyPoints[next][1] == 1 &&
               keyPoints[prev][1] == 1
               ){
                cv::circle(imageMat, contour[keyPoints[i][0]], 2, cv::Scalar(255, 255, 0, 255), 2);
            }
        }
    }
    
    
    cv::Scalar color;
    
    
    /*int fingers = 0;
    std::vector<cv::Vec2i> inHullLines;
    for(int i = 0; i < keyPoints.size(); i++){
        int next = (i + 1)%keyPoints.size();
        int prev = i - 1;
        if(prev < 0){
            prev = (int)keyPoints.size() - 1;
        }
        if(
           keyPoints[i][1] == 1 &&
           keyPoints[next][1] == 0 &&
           keyPoints[prev][1] == 0
        ){
            int angle = GetAngleABC(contour[keyPoints[prev][0]], contour[keyPoints[i][0]], contour[keyPoints[next][0]]);
            if(abs(angle) < 100){
                fingers++;
                if(step != 4){
                    cv::circle(imageMat, contour[keyPoints[i][0]], 3, cv::Scalar(255, 0, 0, 255));
                }
            }
        }
        if(
           keyPoints[i][1] == 0 &&
           keyPoints[next][1] == 1 &&
           keyPoints[prev][1] == 1
       ){
            int angle = GetAngleABC(contour[keyPoints[prev][0]], contour[keyPoints[i][0]], contour[keyPoints[next][0]]);
            //cv::putText(imageMat, std::to_string(abs(angle)), contour[keyPoints[i][0]], cv::FONT_HERSHEY_SIMPLEX, .8, cv::Scalar(255, 255, 0, 255));
            if(abs(angle) < 70){
                if(step != 4){
                    cv::circle(imageMat, contour[keyPoints[i][0]], 3, cv::Scalar(255, 0, 0, 255));
                }
            }
        }
        
        
        if(keyPoints[i][1] == 1){
            color = cv::Scalar(255, 0, 0, 255);
        }else{
            color = cv::Scalar(255, 255, 0, 255);
        }
        if(step != 4){
            cv::circle(imageMat, contour[keyPoints[i][0]], 5, color);
        }
    }*/
    
    //cv::putText(imageMat, std::to_string(fingers), cv::Point(imageMat.cols/2, 50), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0, 0, 0, 255));
    
    
    //Find farest hull points
    if(contour.size() > 0){
        int indexA = 0;
        int indexB = 0;
        float largestDist = 0;
        float dist = 0;
        for(int i = 0; i < hullsIndexes.size(); i++){
            for(int j = 0; j < hullsIndexes.size(); j++){
                if(i != j){
                    dist = lineLength(contour[hullsIndexes[i]], contour[hullsIndexes[j]]);
                    if(dist > largestDist){
                        largestDist = dist;
                        indexA = hullsIndexes[i];
                        indexB = hullsIndexes[j];
                    }
                }
            }
        }
        //cv::line(imageMat, contour[indexA], contour[indexB], cv::Scalar(100, 0, 255, 255));
    }
    
    
    if(step >= 3){
        cv::Mat black = cv::Mat(imageMat.rows, imageMat.cols, imageMat.type(), cv::Scalar(0, 0, 0, 255));
        if(contour.size() > 0){
            if(step != 4){
                cv::polylines(imageMat, contour, true, cv::Scalar(0, 0, 255, 255), 1);
            }
        }
    }
    
    /*std::vector<std::vector<cv::Point>> lines = baseLine(contour, keyPoints, defects);
    for(int i = 0; i < lines.size(); i++){
        cv::line(imageMat, lines[i][0], lines[i][1], cv::Scalar(0, 0, 255, 255), 3);
    }*/
    
    
    //Show hull
    cv::Scalar COLOR_RED = cv::Scalar(255, 0, 0, 255);
    cv::Scalar COLOR_BLUE = cv::Scalar(0, 0, 255, 255);
    cv::Scalar COLOR_YELLOW = cv::Scalar(255, 255, 0, 255);
    cv::Scalar COLOR_GREEN = cv::Scalar(0, 255, 0, 255);
//    cv::Scalar COLOR_WHITE = cv::Scalar(255, 255, 255, 255);
    cv::Scalar COLOR_BLACK = cv::Scalar(0, 0, 0, 255);
    std::vector<cv::Point> hullPoints;
    if(hullsIndexes.size() > 0){
        for(int i = 0; i < hullsIndexes.size(); i++){
            hullPoints.push_back(contour[hullsIndexes[i]]);
        }
        
        if(step != 4){
            //cv::polylines(imageMat, hullPoints, true, cv::Scalar(255, 0, 255, 50), 1);
            
            //cv::fillPoly(imageMat, std::vector<std::vector<cv::Point>>({hullPoints}), COLOR_BLACK);
            //cv::fillPoly(imageMat, std::vector<std::vector<cv::Point>>({contour}), COLOR_WHITE);
        }
    }
    
    //Find center
    if(contour.size() > 0 && hullsIndexes.size() > 0){
        cv::polylines(imageMat, contour, true, COLOR_BLUE, 1);
        int contour_size = (int)contour.size();
        for(int i = 0; i < contour_size; i++){
            cv::circle(imageMat, contour[i], 2, COLOR_BLUE, 2);
        }
        for(int i = 0; i < hullsIndexes.size(); i++){
            cv::circle(imageMat, contour[hullsIndexes[i]], 2, COLOR_GREEN, 2);
        }
        for(int i = 0; i < defectsIndexes.size(); i++){
            cv::circle(imageMat, contour[defectsIndexes[i]], 2, COLOR_YELLOW, 2);
        }
        
        cv::Point center = getCentralPoint(contour);
        cv::Point defectsCenter = getCentralPoint(contour, defectsIndexes);
        cv::Point hullsCenter = getCentralPoint(contour, hullsIndexes);
        
        cv::circle(imageMat, center, 3, COLOR_BLUE, 3);
        cv::circle(imageMat, defectsCenter, 3, COLOR_YELLOW, 3);
        cv::circle(imageMat, hullsCenter, 3, COLOR_GREEN, 3);
        
        
        //Find farest hull line
        float dist, maxDist = 0;
        int next = 0, indexA = 0, indexB = 1;
        std::vector<cv::Vec2f> distances;
        for (int i = 0; i < hullsIndexes.size(); i++) {
            next = (int)hullsIndexes.size() - 1 == i ? 0 : i+1;
            //dist = lineLength(center, pointBetween(contour[hullsIndexes[i]], contour[hullsIndexes[next]]));
            dist = lineLength(center, contour[hullsIndexes[i]]) + lineLength(center, contour[hullsIndexes[next]]);
            distances.push_back(cv::Vec2f(i, dist));
            if(maxDist < dist){
                maxDist = dist;
                indexA = hullsIndexes[i];
                indexB = hullsIndexes[next];
            }
        }
        std::sort(distances.begin(), distances.end(), wayToSortDistances);
        float percent = distances[1][1]/distances[0][1];
        if(percent > .8){
            
            //Finding biggest defect area
            float maxS = 0, s;
            int maxSindexA = 0, maxSindexB = 1;
            for(int i = 0; i < defects.size(); i++){
                s = 0.5*lineLength(contour[defects[i][0]], contour[defects[i][1]])*(defects[i][3]/256.0);
                if(s > maxS){
                    maxS = s;
                    maxSindexA = defects[i][0];
                    maxSindexB = defects[i][1];
                }
            }
            cv::Point maxCenter = pointBetween(contour[maxSindexA], contour[maxSindexB]);
            //cv::putText(imageMat, "MAX", maxCenter, cv::FONT_HERSHEY_PLAIN, 1, COLOR_RED);
            
            //Get min distance hull
            for(int i = 0; i < 2; i++){
                next = getNext(distances[i][0], (int)hullsIndexes.size());
                percent = distances[i][1]/maxDist;
                if(percent > .9){
                    //cv::putText(imageMat, std::to_string(i) + " (" + std::to_string(percent) + ")", pointBetween(contour[hullsIndexes[distances[i][0]]], contour[hullsIndexes[next]]), cv::FONT_HERSHEY_PLAIN, 1, COLOR_BLUE);
                }
            }
            
            next = getNext(distances[0][0], (int)hullsIndexes.size());
            float dist0 = lineLength(pointBetween(contour[hullsIndexes[distances[0][0]]], contour[hullsIndexes[next]]), maxCenter);
            
            next = getNext(distances[1][0], (int)hullsIndexes.size());
            float dist1 = lineLength(pointBetween(contour[hullsIndexes[distances[1][0]]], contour[hullsIndexes[next]]), maxCenter);
            
            if(dist0 < dist1){
                indexB = hullsIndexes[distances[1][0]];
                indexA = hullsIndexes[getNext(distances[1][0], (int)hullsIndexes.size())];
                //cv::putText(imageMat, "FIX", center, cv::FONT_HERSHEY_PLAIN, 1.3, COLOR_RED);
            }
        }
        
        
        //cv::line(imageMat, contour[indexA], contour[indexB], COLOR_RED, 4);
        //cv::line(imageMat, contour[indexA_d], contour[indexB_d], COLOR_YELLOW, 4);
        //cv::line(imageMat, contour[indexA_h], contour[indexB_h], COLOR_GREEN, 4);
        
        
        //Check if line in corner of picture
        int prev = 0, angle = 0;
        if(contour[indexA].y == contour[indexB].y || contour[indexA].x == contour[indexB].x){
            next = getNext(indexA, (int)contour.size());
            prev = getPrev(indexB, (int)contour.size());
            angle = GetAngleABC(contour[indexA], contour[indexB], contour[prev]);
            angle = GetAngleABC(contour[indexB], contour[indexA], contour[next]);
            if(abs(GetAngleABC(contour[indexA], contour[indexB], contour[prev])) == 90){
                indexB = prev;
            }else if(abs(GetAngleABC(contour[indexB], contour[indexA], contour[next])) == 90){
                indexA = next;
            }
        }
        
        //Find indexes in keyPoints
        int indexBInKeyPoints = 0;
        int indexAInKeyPoints = 0;
        std::vector<cv::Vec2i> keyPoints;
        for (int i = 0; i < keyPoints.size(); i++) {
            if(keyPoints[i][0] == indexA){
                indexAInKeyPoints = i;
            }
            if(keyPoints[i][0] == indexB){
                indexBInKeyPoints = i;
            }
        }
        
        //Find next and prev hull points
        int prevHullIndex = getPrev(indexBInKeyPoints, (int)keyPoints.size());
        if(keyPoints[prevHullIndex][1] != 1){
            while(keyPoints[prevHullIndex][1] != 1){
                prevHullIndex = getPrev(prevHullIndex, (int)keyPoints.size());
            }
        }
        
        int nextHullIndex = getNext(indexAInKeyPoints, (int)keyPoints.size());
        if(keyPoints[nextHullIndex][1] != 1){
            while(keyPoints[nextHullIndex][1] != 1){
                nextHullIndex = getNext(nextHullIndex, (int)keyPoints.size());
            }
        }
        
        //Make contour of section B
        std::vector<int> indexesForSectionB;
        std::vector<cv::Point> contourB;
        contourB.push_back(contour[indexB]);
        contourB.push_back(contour[keyPoints[prevHullIndex][0]]);
        if(contour_size > 2){
            indexesForSectionB = getIndexesBetween(keyPoints[prevHullIndex][0], indexB, contour_size);
            for(int i = 0; i < indexesForSectionB.size(); i++){
                contourB.push_back(contour[indexesForSectionB[i]]);
            }
        }
        
        //Make contour of section A
        std::vector<int> indexesForSectionA;
        std::vector<cv::Point> contourA;
        contourA.push_back(contour[keyPoints[nextHullIndex][0]]);
        contourA.push_back(contour[indexA]);
        if(contour_size > 2){
            indexesForSectionA = getIndexesBetween(indexA, keyPoints[nextHullIndex][0], contour_size);
            for(int i = 0; i < indexesForSectionA.size(); i++){
                contourA.push_back(contour[indexesForSectionA[i]]);
            }
        }
        
        if(contourA.size() > 2){
            //std::vector<cv::Point> proectionContourA = getDiagram(contourA, 0, 1);
        }
        if(contourB.size() > 2){
            //std::vector<cv::Point> proectionContourB = getDiagram(contourB, 0, 1);
        }
        
        //std::cout << "A: " << contourA.size() << " B: " << contourB.size() << std::endl;
        
        float minDist = -1;
        int minA = 0, minB = 0;
        for(int i = 0; i < indexesForSectionA.size(); i++){
            for(int j = 0; j < indexesForSectionB.size(); j++){
                dist = lineLength(contour[indexesForSectionA[i]], contour[indexesForSectionB[j]]);
                if(minDist < 0 || minDist > dist){
                    minDist = dist;
                    minA = indexesForSectionA[i];
                    minB = indexesForSectionB[j];
                }
            }
        }
        /*if(minA > 0 && minB > 0){
            cv::line(imageMat, contour[minA], contour[minB], cv::Scalar(255, 255, 0, 255), 4);
        }*/
        
        //Step 1:
        //Finding defects on nearst 2 points
        std::string sA = "1";
        std::string sB = "1";
        int index = 0, next2, prev2, size = (int)contour.size();
        int indexA1 = -1, indexB1 = -1;
        for(int i = 0; i < keyPoints.size(); i++){
            if(keyPoints[i][1] == 0){
                index = keyPoints[i][0];
                next = getNext(indexA, size);
                prev = getPrev(indexB, size);
                next2 = getNext(next, size);
                prev2 = getPrev(prev, size);
                
                if(index == next || index == next2){
                    if(abs(GetAngleABC(contour[indexB], contour[indexA], contour[index])) > 25){
                        indexA1 = index;
                        
                    }
                }else if(index == prev || index == prev2){
                    if(abs(GetAngleABC(contour[indexA], contour[indexB], contour[index])) > 25){
                        indexB1 = index;
                    }
                }
            }
        }
        
        //Step 2:
        //Finding near defect
        if(indexA1 < 0){
            int angle;
            next = getNext(indexAInKeyPoints, (int)keyPoints.size());
            next2 = getNext(next, (int)keyPoints.size());
            if(keyPoints[next][1] == 0 && keyPoints[next2][1] == 1){
                angle = abs(GetAngleABC(contour[indexB], contour[indexA], contour[keyPoints[next][0]]));
                if(angle > 25){
                    indexA1 = keyPoints[next][0];
                    sA = "2";
                }
            }
        }
        
        if(indexB1 < 0){
            int angle;
            prev = getPrev(indexBInKeyPoints, (int)keyPoints.size());
            prev2 = getPrev(prev, (int)keyPoints.size());
            if(keyPoints[prev][1] == 0 && keyPoints[prev2][1] == 1){
                angle = abs(GetAngleABC(contour[indexA], contour[indexB], contour[keyPoints[prev][0]]));
                if(angle > 25){
                    indexB1 = keyPoints[prev][0];
                    sB = "2";
                }
            }
        }
        
        //Step 3:
        //Just take near hull
        if(indexA1 < 0){
            next = getNext(indexAInKeyPoints, (int)keyPoints.size());
            indexA1 = keyPoints[next][0];
            sA = "3";
        }
        if(indexB1 < 0){
            prev = getPrev(indexBInKeyPoints, (int)keyPoints.size());
            indexB1 = keyPoints[prev][0];
            sB = "3";
        }
        
        //Step 4:
        //Check if min line better
        float angleBetweenAB_MinA = GetAngleABC(contour[indexB], contour[indexA], contour[minA]);
        float angleBetweenBA_MinB = GetAngleABC(contour[indexA], contour[indexB], contour[minB]);
        float angleBetweenAB_A1 = GetAngleABC(contour[indexB], contour[indexA], contour[indexA1]);
        float angleBetweenBA_B1 = GetAngleABC(contour[indexA], contour[indexB], contour[indexB1]);
        if(fabsf(angleBetweenAB_A1) > fabsf(angleBetweenAB_MinA)){
            indexA1 = minA;
            sA = "4";
        }
        if(fabsf(angleBetweenBA_B1 - 90) > fabsf(angleBetweenBA_MinB - 90)){
            indexB1 = minB;
            sA = "4";
        }
        
        //Step 5:
        //Check if result is impossible
        int dx = contour[indexA].x - contour[indexA1].x;
        int dy = contour[indexA].y - contour[indexA1].y;
        float angleBetweenB1A1_AB = GetAngleABC(contour[indexB], contour[indexA], cv::Point(contour[indexB1].x + dx, contour[indexB1].y + dy));
        if(fabsf(angleBetweenB1A1_AB) > 45){
            indexA1 = -1;
            indexB1 = -1;
        }
        //cv::putText(imageMat, std::to_string(angleBetweenB1A1_AB), center, cv::FONT_HERSHEY_PLAIN, 1.3, COLOR_RED);
        
        
        
        if(step != 4){
            if(indexA > 0){
                //cv::putText(imageMat, "A", contour[indexA], cv::FONT_HERSHEY_SIMPLEX, .6, cv::Scalar(255, 0, 0, 255));
            }
            if(indexA1 > 0){
                //cv::putText(imageMat, "A1-"+sA, contour[indexA1], cv::FONT_HERSHEY_SIMPLEX, .6, cv::Scalar(255, 100, 0, 255));
            }
        }
        if(step != 4){
            /*
            if(indexB > 0){
                cv::putText(imageMat, "B", contour[indexB], cv::FONT_HERSHEY_SIMPLEX, .6, cv::Scalar(0, 255, 0, 255));
            }
            if(indexB1 > 0){
                cv::putText(imageMat, "B1-"+sB, contour[indexB1], cv::FONT_HERSHEY_SIMPLEX, .6, cv::Scalar(100, 255, 0, 255));
            }
            */
        }
        
        if(step == 4){
            std::vector<cv::Point> base = {contour[indexA], contour[indexB]};
            if(indexB1 > 0){
                base.push_back(contour[indexB1]);
                indexB = indexB1;
            }
            if(indexA1 > 0){
                base.push_back(contour[indexA1]);
                indexA = indexA1;
            }
            cv::line(imageMat, contour[indexA], contour[indexB], COLOR_RED, 5);
            //cv::polylines(imageMat, base, true, cv::Scalar(100, 255, 0, 100), 3);
        }
        
        
        //=== Next Section ====
        //Step 1:
        //Farest point from red line
        maxDist = 0;
        int indexC = -1;
        float maxAngle = 0;
        float an;
        cv::Point centerAB = pointBetween(contour[indexA], contour[indexB]);
        for(int i = 0; i < defectsIndexes.size(); i++){
            if(i != indexA && i != indexB){
                dist = lineLength(centerAB, contour[defectsIndexes[i]]);
                an = GetAngleABC(contour[indexB], contour[indexA], contour[defectsIndexes[i]]);
                if(dist > maxDist){
                    maxDist = dist;
                    maxAngle = an;
                    indexC = defectsIndexes[i];
                }
            }
        }
        if(indexC >= 0){
            //cv::line(imageMat, centerAB, contour[indexC], COLOR_GREEN, 4);
            float prop = 1 - (1.7 - lineLength(centerAB, contour[indexC])/lineLength(contour[indexA], contour[indexB]));
            cv::putText(imageMat, std::to_string(prop), center, cv::FONT_HERSHEY_PLAIN, 1.3, COLOR_BLACK);
            
            cv::Point A = contour[indexA];
            cv::Point B = contour[indexB];
            cv::Point C = contour[indexC];
            
//            float halfDX = ((A.x - B.x)/2)*prop;
//            float halfDY = ((A.y - B.y)/2)*prop;
            
//            cv::Point D = cv::Point(C.x - halfDX, C.y - halfDY);
//            cv::Point E = cv::Point(C.x + halfDX, C.y + halfDY);
            //cv::line(imageMat, M1, M2, COLOR_YELLOW, 4);
            
            //cv::polylines(imageMat, std::vector<cv::Point>({A, B, M1, M2}), true, COLOR_RED, 5);
            //if(abs(abs(GetAngleABC(B, D, E)) - 90) < 40.0){
            //    cv::fillPoly(imageMat, std::vector<std::vector<cv::Point>>({{A, B, D, E}}), COLOR_GREEN);
            //}else{
                cv::line(imageMat, centerAB, contour[indexC], COLOR_GREEN, 4);
            //}
        }
    }
    
    //Работа с охватывающим прямоугольником
    /*
    if(contour.size() > 0){
        //Находим прямоугольник и делаем из него контур
        //Заодно сохраняем длины его сторон
        cv::RotatedRect rectArea = cv::minAreaRect(contour);
        cv::Point2f vtx[4];
        rectArea.points(vtx);
        std::vector<float> lineSizes(4);
        std::vector<cv::Point> rectContour(4);
        for(int t = 0; t < 4; t++){
            lineSizes[t] = lineLength(vtx[t], vtx[(t+1)%4]);
            rectContour[t] = vtx[t];
        }
        
        //Делим прямоугольник пополам поперек
        cv::Point
            p1 = rectContour[0],
            p2 = rectContour[1],
            p3 = rectContour[2],
            p4 = rectContour[3];
        
        std::vector<std::vector<cv::Point>> sectors = {
            {p1, p2, p3},
            {p2, p3, p4},
            {p3, p4, p1},
            {p2, p1, p4},
            {p1, pointBetween(p1, p3), p2},
            {p2, pointBetween(p1, p3), p3},
            {p3, pointBetween(p1, p3), p4},
            {p4, pointBetween(p1, p3), p1},
        };
        
        if(lineSizes[1] > lineSizes[0]){
            sectors.push_back({pointBetween(p3, p2), pointBetween(p1, p4), p1, p2});
            sectors.push_back({pointBetween(p1, p4), pointBetween(p3, p2), p3, p4});
        }else{
            sectors.push_back({pointBetween(p2, p1), pointBetween(p4, p3), p4, p1});
            sectors.push_back({pointBetween(p3, p4), pointBetween(p1, p2), p2, p3});
        }*/
        
        //Считаем в каком прямоугольнике больше узлов контура
        /*
        std::vector<int> pointsInSectors(sectors.size(), 0);
        for (int j = 0; j < hullPoints.size(); j++) {
            for(int k = 0; k < sectors.size(); k++) {
                if(cv::pointPolygonTest(sectors[k], hullPoints[j], false) > 0){
                    pointsInSectors[k]++;
                }
            }
        }
        int maxPointsNumber = 0;
        int maxPointsSectorIndex = 0;
        for (int i = 0; i < pointsInSectors.size(); i++) {
            if(maxPointsNumber < pointsInSectors[i]){
                maxPointsNumber = pointsInSectors[i];
                maxPointsSectorIndex = i;
            }
        }
        
        int i = maxPointsSectorIndex;
        cv::Point pointA, pointB;
        if(sectors[i].size() == 4){
            pointA = pointBetween(sectors[i][0], sectors[i][1]);
            pointB = pointBetween(sectors[i][2], sectors[i][3]);
        }else{
            pointA = pointBetween(sectors[i][0], sectors[i][2]);
            pointB = sectors[i][1];
        }
        
        cv::line(imageMat, pointA, pointB, cv::Scalar(255, 0, 0, 255));
        cv::circle(imageMat, pointB, 4, cv::Scalar(255, 0, 0, 255));
        
        cv::Vec4f line;
        cv::fitLine(hullPoints, line, cv::DIST_HUBER, 0, 0.01, 0.01);
        cv::circle(imageMat, cv::Point(line[2], line[3]), 3, cv::Scalar(255, 100, 0, 255));
         */
        
            //cv::polylines(imageMat, sectors[i], true, cv::Scalar(0, 0, 255, 255), 1);
            //cv::putText(imageMat, std::to_string(pointsInSectors[i]), pointBetween(sectors[i][0], sectors[i][1]), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0, 0, 255, 255));
        
        
        
        /*
        cv::Point pointA, pointB;
        for(int i = 0; i < 4; i++ ){
            cv::line(imageMat, vtx[i], vtx[(i+1)%4], cv::Scalar(0, 255, 0, 255));
            
            pointA = vtx[i];
            pointB = vtx[(i+1)%4];
            for(int j = 0; j < contour.size(); j++){
                if(cv::pointPolygonTest(rectContour, contour[j], false)){
                    cv::circle(imageMat, contour[j], 2, cv::Scalar(255, 20, 100, 255));
                }
                if(lineSizes[i] > minRectSize + 5){
                    if(isPointOnLine(contour[j], pointA, pointB)){
                        cv::circle(imageMat, contour[j], 2, cv::Scalar(255, 100, 0, 255));
                    }
                }
            }
        }
        */
        
    //}
    
    
    
    return MatToUIImage(imageMat);
}
 
+(UIImage *) makeGrayFromImage:()image temp:(UIImage *)temp
{
    cv::Mat img_object, img_scene;
    UIImageToMat(image, img_scene);
    UIImageToMat(temp, img_object);
    
    std::string detectorType;
    
    std::vector<cv::KeyPoint> scene_keypoint, object_keypoint;
    //int type = 40;
    cv::Ptr<cv::FastFeatureDetector> detector = cv::FastFeatureDetector::create( );
    detector->detect(img_scene, scene_keypoint);
    detector->detect(img_object, object_keypoint);
    
    cv::Ptr<cv::DescriptorExtractor> extractor = cv::ORB::create();
    cv::Mat descriptors_object, descriptors_scene;
    extractor->compute(img_object, object_keypoint, descriptors_object);
    extractor->compute(img_scene, scene_keypoint, descriptors_scene);
    
    cv::FlannBasedMatcher matcher;
    std::vector<cv::DMatch> matches;
    matcher.match(descriptors_object, descriptors_scene, matches);
    std::cout << matches.size() << std::endl;
    
    double max_dist = 0; double min_dist = 100;
    
    for( int i = 0; i < descriptors_object.rows; i++ ){
        double dist = matches[i].distance;
        if( dist < min_dist ) min_dist = dist;
        if( dist > max_dist ) max_dist = dist;
    }
    
    std::vector<cv::DMatch> good_matches;
    for( int i = 0; i < descriptors_object.rows; i++ ){
        if( matches[i].distance < 3*min_dist ){
            good_matches.push_back( matches[i]);
        }
    }
    
    return MatToUIImage(img_scene);
}

/*
+(UIImage *) makeContoursFromImage:()image
{
    cv::Mat imageMat;
    cv::RNG rng(12345);
    std::vector<cv::Vec4i> hierarchy;
    UIImageToMat(image, imageMat);
    
    cv::Mat imageDX, image2, image3;
    //imageMat.convertTo(imageMat, CV_8UC1);
    cv::threshold(imageMat, image2, 140, 255, cv::THRESH_BINARY);
    image2.convertTo(image3, CV_8UC1);
    
    std::cout << image3.depth() << std::endl;
    std::cout << CV_MAKETYPE(CV_8U, 1) << std::endl;
    
    std::vector<std::vector<cv::Point>> contours;
    
    cv::findContours(image3, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
    for( size_t i = 0; i< contours.size(); i++ )
    {
        cv::Scalar color = cv::Scalar( rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255) );
        cv::drawContours( image3, contours, (int)i, color, 2, 8, hierarchy, 0, cv::Point() );
    }
    //Sobel( imageMat, imageDX, CV_32F, 0, 1, 3, 1.0 / 255.0 );
    
    
    return MatToUIImage(image3);
}
 */
@end
