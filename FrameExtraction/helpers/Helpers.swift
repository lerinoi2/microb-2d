//
//  Helpers.swift
//  FrameExtraction
//
//  Created by Ефремов Михаил on 09.07.2018.
//  Copyright © 2018 bRo. All rights reserved.


extension UIColor {
    
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"#%06x", rgb)
    }
}

extension UIView {
    
    func applyGradient(colors: [UIColor]) -> Void {
        self.applyGradient(colors: colors, locations: nil)
    }
    
    func applyGradient(colors: [UIColor], angle: CGFloat, corner: CGFloat? = nil) -> Void {
        self.applyGradient(colors: colors, locations: nil, angle: angle, corner: corner)
    }
    
    func applyGradient(colors: [UIColor], locations: [NSNumber]?, angle: CGFloat? = nil, corner: CGFloat? = nil) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colors.map { $0.cgColor }
        gradient.locations = locations
        gradient.name = "gradient"
        if (angle != nil) {
            let x: CGFloat! = angle! / 360.0
            let pi = CGFloat.pi
            let a = pow(sinf(Float(2.0 * pi * ((x + 0.75) / 2.0))),2.0);
            let b = pow(sinf(Float(2*pi*((x+0.0)/2))),2);
            let c = pow(sinf(Float(2*pi*((x+0.25)/2))),2);
            let d = pow(sinf(Float(2*pi*((x+0.5)/2))),2);
            
            gradient.endPoint = CGPoint(x: CGFloat(c),y: CGFloat(d))
            gradient.startPoint = CGPoint(x: CGFloat(a),y:CGFloat(b))
        }
        if (corner != nil) {
            gradient.cornerRadius = corner!
        }
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func dropShadowBot() {
        let shadowPath = UIBezierPath()
        shadowPath.move(to: CGPoint(x: 0, y: self.bounds.height))
        shadowPath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
        shadowPath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height + 5))
        shadowPath.addLine(to: CGPoint(x: 0, y: self.bounds.height + 5))
        shadowPath.close()
        
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.6
        layer.masksToBounds = false
        layer.shadowPath = shadowPath.cgPath
        layer.shadowRadius = 10
    }

    func rotate(_ angle: CGFloat) {
        let radians = angle / 180.0 * CGFloat(Double.pi)
        let rotation = CGAffineTransform(rotationAngle: radians)
        self.transform = rotation
    }
}


public func apiRequest(action: String, method: String = "POST", dataString: String? = nil, sender: UIViewController? = nil, success: ((_ data: Data) throws ->Void)? = nil, complete: (()->Void)? = nil) {
    let urlString = "https://apigerms.tinkerr.io/v1/\(action)/?token=rqFtYHrxn4xntfuSqkNTBUeEkDcDK4nH75BfyZjzu6nn6JuF7tYvZjpAr67rmGF7SbTEkTEWvAPuygWS7pYsQYAKmss8QEZtgjQc"
    let url = URL(string: urlString)!
    var request = URLRequest(url: url)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpMethod = method
    if let requestString = dataString?.data(using: .utf8) {
        request.httpBody = requestString
    }
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let _ = data, error == nil else {                                                 // check for fundamental networking error
            alertWithTitle(title: "Network error", message: "Check your internet connection")
            return
        }
        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
            if httpStatus.statusCode == 403 {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : Any]
                    let message = json["message"] as! String
                    alertWithTitle(title: "ERROR", message: message)
                } catch let error as NSError {
                    print("Failed to parse error: \(error.localizedDescription)")
                    print(url)
                }
            } else if httpStatus.statusCode == 401 {
                alertWithTitle(title: "Network error", message: "Check your internet connection")
            } else if httpStatus.statusCode == 409 {
                alertWithTitle(title: "ERROR", message: "Your app version is out of date. Please update now from AppStore")
            } else {
                alertWithTitle(title: "Server error", message: "We are sorry, try again later")
            }
            print(url)
        }
        if success != nil {
            do {
                try success!(data!)
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
                print(url)
            }
        }
        if complete != nil {
            complete!()
        }
    }
    task.resume()
}

func alertWithTitle(title: String!, message: String, toFocus:UITextField? = nil) {
    DispatchQueue.main.async {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel,handler: {_ in
            if toFocus != nil {
                toFocus?.becomeFirstResponder()
            }
        })
        alert.addAction(action)
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.present(alert, animated: true, completion:nil)
        }
    }
}

func getImage(_ url: URL, complete: @escaping ((_ image: UIImage?)->Void)) {
    let session = URLSession(configuration: .default)
    //creating a dataTask
    let getImageFromUrl = session.dataTask(with: url) { (data, response, error) in
        var image: UIImage?
        //if there is any error
        if let e = error {
            //displaying the message
            print("Error Occurred: \(e)")
            
        } else {
            //in case of now error, checking wheather the response is nil or not
            if (response as? HTTPURLResponse) != nil {
                
                //checking if the response contains an image
                guard let imageData = data else {
                    print("Image file is currupted")
                    return
                }
                if let newImage = UIImage(data: imageData){
                    image = newImage
                } else {
                    print("No photo")
                }
            } else {
                print("No response from server")
            }
        }
        complete(image)
    }
    
    //starting the download task
    getImageFromUrl.resume()
}
