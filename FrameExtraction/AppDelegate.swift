//
//  AppDelegate.swift
//  FrameExtraction
//
//  Created by Bobo on 29/12/2016.
//  Copyright © 2016 bRo. All rights reserved.
//

import UIKit

let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sbPopUpID") as! PopUpViewController
let popOverVCRM = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sbPopUpIDMR") as! PopOverVCRM
let contactUs = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contactUsID") as! ContactUs
let textVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "textVcID") as! TextVC
let sendMsgVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SendMsgVCID") as! SendMsgVC
let firstLaunchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FirstLaunchVCID") as! FirstLaunchVC
let handCleanVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HandCleanVCID") as! HandCleanVC
let shopVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShopVCID") as! ShopVC

let defaults = UserDefaults.standard


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}

