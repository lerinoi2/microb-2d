//
//  PopOverVCRM.swift
//  FrameExtraction
//
//  Created by Ефремов Михаил on 12.07.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import UIKit

class PopOverVCRM: UIViewController {
    
    @IBOutlet var darkFrame: UIImageView!
    @IBOutlet var closeButton: UIButton!
    
    @IBOutlet var rightOptions: UICollectionView!
    @IBOutlet var heightList: NSLayoutConstraint!
    
    
    @IBAction func closeButtonAction(_ sender: Any) {
        removeAnimate()
    }
    
    var  rightOptionsList: [RightOption] = []
    
    let cellID = "rightOptionID"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiRequest(action: "info/list", sender: self, success: { data in
            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : Any]
            if let info = json["info"] as? [AnyObject] {
                info.forEach({ (infoTab) in
                    let option = RightOption(id: infoTab["id"] as! Int,
                                             title: infoTab["name"] as! String,
                                             text: infoTab["text"] as! String)
                    self.rightOptionsList.append(option)
                })
            }
        }) {
            DispatchQueue.main.async {
                self.rightOptions.reloadData()
                let listCount = self.rightOptionsList.count
                let hght: CGFloat = CGFloat(60 + listCount * 72)
                self.heightList.constant = hght
            }
        }
        
        rightOptions.layer.cornerRadius = 30
        
        addChildViewController(textVC)
        textVC.view.frame = view.frame
        textVC.view.frame.origin.x = view.frame.width
        view.addSubview(textVC.view)
        
        rightOptions.delegate = self
        rightOptions.dataSource = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        darkFrame.isUserInteractionEnabled = true
        darkFrame.addGestureRecognizer(tapGestureRecognizer)
        

    }
    
    @IBOutlet var tap: UIImageView!
    
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        removeAnimate()
    }
    
    var showBtn : UIView!
    
    func showAnimate(_ btn: UIView)
    {
        showBtn = btn
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.frame.origin.x = 0
            self.showBtn.frame.origin.x = -self.view.frame.width + 270
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
                    self.darkFrame.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                }, completion: nil)
            }
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.darkFrame.backgroundColor = UIColor.black.withAlphaComponent(0)
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.view.frame.origin.x = self.view.frame.width + 20
                    self.showBtn.frame.origin.x = self.view.frame.width - 64
                }, completion:nil)
            }
        });
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension PopOverVCRM: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 62)
    }
}

extension PopOverVCRM: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.rightOptionsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.rightOptions.dequeueReusableCell(withReuseIdentifier: self.cellID, for: indexPath) as! RightOptionCell

        
        let option = rightOptionsList[indexPath.row]
        cell.optionID = option.id
        
        if !cell.setuped {
            cell.setupCell()
        }
        
        if (indexPath.row + 1 == self.rightOptionsList.count){
            cell.border.removeFromSuperlayer()
        }
        
        cell.titel.text = option.title
        cell.texts = option.text
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.rightOptions.cellForItem(at: indexPath) as! RightOptionCell

        let text: String = cell.texts
        
        textVC.showAnimate(text)
    }

}

class RightOptionCell: UICollectionViewCell{
    var optionID: Int!
    let border = CALayer()
    let width = CGFloat(1.0)
    var texts: String!
    
    @IBOutlet var titel: UILabel!
    
    var setuped = false
    func setupCell(){
        setuped = true
        border.borderColor = UIColor.gray.withAlphaComponent(0.2).cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

class RightOption: NSObject {
    var id: Int!
    var title: String!
    var text: String!
    
    init(id:Int, title:String, text:String) {
        self.id = id
        self.title = title
        self.text = text
    }
}
