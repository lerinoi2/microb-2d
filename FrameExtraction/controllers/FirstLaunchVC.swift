//
//  FirstLaunchVC.swift
//  FrameExtraction
//
//  Created by Ефремов Михаил on 13.07.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import UIKit

class FirstLaunchVC: UIViewController {

    @IBOutlet var submitBtn: UIButton!
    @IBOutlet var submitView: UIView!
    @IBOutlet var scanBlock: UIView!
    @IBOutlet var shadowBlock: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scanBlock.applyGradient(colors: [UIColor(hexString: "#f59b43"), UIColor(hexString: "#f88529")], locations: [0, 1], angle: 180)
        submitView.applyGradient(colors: [UIColor(hexString: "#f86d29"), UIColor(hexString: "#ffa650")], locations: [0, 1], angle: 125, corner:20)
        shadowBlock.applyGradient(colors: [UIColor.clear, UIColor(hexString: "#f88529")], locations: [0, 1], angle: 180)
        
    }
    

    @IBAction func startBtn(_ sender: Any) {
        showAnimate()
    }
    
    func showAnimate()
    {

        UIView.animate(withDuration: 1.5, delay: 0, options: .curveLinear, animations: {
            self.scanBlock.frame.origin.y = -20
            self.shadowBlock.frame.origin.y = -20
        }, completion:{ finished in
            if finished {
                self.shadowBlock.frame.origin.y = -self.shadowBlock.frame.height + 15 - 20
                self.shadowBlock.rotate(180)
                UIView.animate(withDuration: 1.5, delay: 0.0, options: .curveLinear, animations: {
                    self.scanBlock.frame.origin.y = self.view.frame.height - 15 + 20
                    self.shadowBlock.frame.origin.y = self.view.frame.height - self.shadowBlock.frame.height - 20 + 25
                }, completion:{ finished in
                    if finished {
                        self.shadowBlock.frame.origin.y = 722 + 20
                        self.shadowBlock.rotate(360)
                        self.showAnimate()
                    }
                })
            }
        })

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
