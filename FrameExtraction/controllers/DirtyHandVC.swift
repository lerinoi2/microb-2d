//
//  DirtyHandVC.swift
//  FrameExtraction
//
//  Created by Ефремов Михаил on 13.07.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import UIKit

class DirtyHandVC: UIViewController {

    @IBOutlet var block: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        block.applyGradient(colors: [UIColor(hexString: "#f86d29"), UIColor(hexString: "#ffa650")], locations: [0, 1], angle: 125, corner:20)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        darkFrame.isUserInteractionEnabled = true
        darkFrame.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        removeAnimate()
    }
    
    
    var lView : UIView!
    var rView : UIView!
    var bView : UIView!
    
    func showAnimate(_ left: UIView, _ right: UIView, _ bot: UIView)
    {
        lView = left
        rView = right
        bView = bot
        view.alpha = 1.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.frame.origin.x = 0
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
                    self.darkFrame.image = UIImage(named: "stars")
                    self.darkFrame.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                }, completion: nil)
            }
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.darkFrame.alpha = 0
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.view.frame.origin.x = -self.view.frame.width - 50
                    self.lView.frame.origin.x = -30
                    self.rView.frame.origin.x = self.view.frame.width - 64
                    self.bView.frame.origin.y = self.bView.frame.origin.y - self.bView.frame.height
                }, completion:nil)
            }
        });
    }

}
