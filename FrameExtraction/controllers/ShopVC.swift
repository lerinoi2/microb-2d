//
//  ShopVC.swift
//  FrameExtraction
//
//  Created by Ylee mini on 17.07.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import UIKit

class ShopVC: UIViewController {
    
    var hIMG: CGFloat!
    var SizePic: CGFloat!
    var padPic: CGFloat!

    @IBOutlet var shopOptions: UICollectionView!
    
    @IBAction func closeBtn(_ sender: Any) {
        removeAnimate()
    }
    
    var  shopOptionsList: [ShopOption] = []
    
    @IBOutlet var topBlock: UIView!
    
    let cellID = "shopOptionID"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shopOptionsList = [
            ShopOption(id: 1, appleId: "1", mainPicture: UIImage(named: "photo-1")!, name: "Standart", text: "Free"),
            ShopOption(id: 5, appleId: "2", name: "Germs on Fire", textPrice: "$2.99", mainPicture: UIImage(named: "donat")!),
//            ShopOption(id: 3, mainPicture: UIImage(named: "photo-1")!, name: "Standart3", text: "free3"),
//            ShopOption(id: 4, mainPicture: UIImage(named: "photo-1")!, name: "Standart4", text: "free4"),
//            ShopOption(id: 5, name: "Standart5", textPrice: "1", mainPicture: UIImage(named: "photo-1")!),
        ]

        topBlock.dropShadowBot()
        shopOptions.delegate = self
        shopOptions.dataSource = self
        shopOptions.backgroundColor = .clear
        
        hIMG = (self.view.frame.width - 42) / 1.9375
        SizePic = (self.view.frame.width - 42) / 4.862
        padPic = (self.view.frame.width - 42) / 4.862
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func showAnimate()
    {
        view.alpha = 1.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.frame.origin.x = 0
        }, completion: nil)
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.view.frame.origin.x = -self.view.frame.width
        }, completion: nil)
    }
    
}

extension ShopVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let h = (self.view.frame.width - 42) / 1.385
        let w = self.view.frame.width - 42
        return CGSize(width: w, height: h)
    }
}

extension ShopVC: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.shopOptionsList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.shopOptions.dequeueReusableCell(withReuseIdentifier: self.cellID, for: indexPath) as! ShopOptionCell
        
        cell.contentView.layer.cornerRadius = 10
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true;
        
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width:0,height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false;
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        let option = shopOptionsList[indexPath.row]
        cell.optionID = option.id
        
        if !cell.setuped {
            cell.setupCell()
            cell.imgHeight.constant = self.hIMG
            cell.hPrice.constant = self.SizePic
            cell.wPrice.constant = self.SizePic
//
            cell.pricePadding.constant = (cell.priceBlock.frame.height / 2) + 10
        }
        
        if (option.textPrice != nil){
            cell.optionName.text = option.name
            cell.optionTextPicPrice.text = option.textPrice
            cell.optionPicture.image = option.mainPicture
            
            cell.optionTextPrice.alpha = 0
            cell.optionPicPrice.layer.cornerRadius = self.SizePic / 2
            cell.optionPicPrice.frame.size = CGSize(width: self.SizePic, height: self.SizePic)
            
            cell.optionPicPrice.applyGradient(colors: [UIColor(hexString: "#f86d29"), UIColor(hexString: "#ffa650")], angle: -125, corner: self.SizePic / 2)
            cell.optionPicPrice.layer.shadowColor = UIColor.gray.cgColor
            cell.optionPicPrice.layer.shadowOffset = .zero
            cell.optionPicPrice.layer.shadowRadius = 10
            cell.optionPicPrice.layer.shadowOpacity = 0.7
        } else {
            cell.optionPicPrice.alpha = 0
            cell.optionTextPicPrice.alpha = 0
            cell.optionPicture.image = option.mainPicture
            cell.optionName.text = option.name
            cell.optionTextPrice.text = option.picPrice
        }
        return cell
    }
}

class ShopOptionCell: UICollectionViewCell{
    var optionID: Int!
    
    @IBOutlet var optionPicture: UIImageView!
    @IBOutlet var optionName: UILabel!
    
    @IBOutlet var imgHeight: NSLayoutConstraint!
    
    @IBOutlet var priceBlock: UIView!
    @IBOutlet var pricePadding: NSLayoutConstraint!
    @IBOutlet var hPrice: NSLayoutConstraint!
    @IBOutlet var wPrice: NSLayoutConstraint!
    
    
    @IBOutlet var optionPicPrice: UIView!
    @IBOutlet var optionTextPicPrice: UILabel!
    
    @IBOutlet var optionTextPrice: UILabel!
    
    var setuped = false
    func setupCell(){
        setuped = true
    }
}

class ShopOption: NSObject {
    var id: Int!
    var mainPicture: UIImage!
    var name: String!
    var textPrice: String!
    var picPrice: String!
    var appleId: String!
    
    
    init(id:Int, appleId:String = "", mainPicture:UIImage, name:String, text:String) {
        self.id = id
        self.appleId = appleId
        self.mainPicture = mainPicture
        self.name = name
        self.picPrice = text
    }
    
    init(id:Int, appleId:String = "", name:String, textPrice:String, mainPicture:UIImage) {
        self.id = id
        self.appleId = appleId
        self.name = name
        self.textPrice = textPrice
        self.mainPicture = mainPicture
    }
}
