//
//  SendMsgVC.swift
//  FrameExtraction
//
//  Created by Ефремов Михаил on 13.07.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import UIKit

class SendMsgVC: UIViewController {
    
    
    @IBOutlet var darkFrame: UIImageView!
    @IBOutlet var block: UIView!
    
    @IBAction func closeBtn(_ sender: Any) {
        removeAnimate()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        block.applyGradient(colors: [UIColor(hexString: "#f86d29"), UIColor(hexString: "#ffa650")], locations: [0, 1], angle: 125, corner:20)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        darkFrame.isUserInteractionEnabled = true
        darkFrame.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        removeAnimate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showAnimate()
    {
        view.alpha = 1.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.frame.origin.x = 0
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
                    self.darkFrame.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                }, completion: nil)
            }
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.darkFrame.backgroundColor = UIColor.black.withAlphaComponent(0)
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.view.frame.origin.x = -self.view.frame.width - 100
                }, completion:nil)
            }
        });
    }

}
