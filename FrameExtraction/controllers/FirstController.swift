//
//  FirstController.swift
//  FrameExtraction
//
//  Created by Ефремов Михаил on 06.07.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import Foundation
import UIKit

class FirstController: UIViewController, FrameExtractorDelegate {

    var frameExtractor: FrameExtractor!
    
    func captured(image: UIImage) {
        DispatchQueue.main.async {
            self.microbeCamera.image = image
        }
    }

    @IBOutlet var microbeLM: UIView!
    @IBOutlet var microbeLMBtn: UIButton!
    
    @IBAction func microbeOpenLM(_ sender: Any) {
        popOverVC.showAnimate(microbeLM)
    }
    
    @IBAction func microbeRMBtnAction(_ sender: Any) {
        popOverVCRM.showAnimate(microbeRM)
    }
    
    
    @IBOutlet var microbeRMBtn: UIButton!
    @IBOutlet var bottomBar: UIView!
    @IBOutlet var bottomBarBackgroud: UIView!
    @IBOutlet var microbeScan: UIButton!
    @IBOutlet var microbeBtn: UIButton!
    @IBOutlet var microbeShare: UIButton!

    @IBOutlet var microbeRM: UIView!
    @IBOutlet var microbeCamera: UIImageView!

    var optionShown = false
    
    let microbeImg = UIImageView()
    let microbeScanImg = UIImageView()
    let microbeShareImg = UIImageView()
    
    @IBAction func microbeBtnAction(_ sender: Any) {
        var opacity = 0
        self.optionShown = !self.optionShown
        DispatchQueue.main.async {
            if !self.optionShown {
                self.microbeOptionPos.constant = -self.microbeOptionsHeight.constant
                // select clean
            } else {
                self.tappedIMG = 0
                self.microbeImg.backgroundColor = .white
                self.microbeImg.image = UIImage(named: "MicrobeHover")
                opacity = 1
                self.microbeOptionPos.constant = 0
            }
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                self.microbeOptions.alpha = CGFloat(opacity)
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    @IBOutlet var microbeOptions: UICollectionView!
    @IBOutlet var microbeOptionsHeight: NSLayoutConstraint!
    @IBOutlet var microbeOptionPos: NSLayoutConstraint!
    
    var microbeOptionsList: [MicrobeOption] = []
    
    let cellID = "microbeOptionID"
  
//    @IBAction func takeScreen(_ sender: Any) {
//        takeScreenshot(view: view)
//        microbeCamera.alpha = 0
//        UIView.animate(withDuration: 0.3,
//                       delay: 0,
//                       options: [.curveLinear,],
//                       animations: { self.microbeCamera.alpha = 1.0 },
//                       completion: nil)
//    }
    
//    func takeScreenshot(view: UIView) -> UIImageView {
//        UIGraphicsBeginImageContext(view.frame.size)
//        view.layer.render(in: UIGraphicsGetCurrentContext()!)
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
//
//        return UIImageView(image: image)
//    }
    
    
    @IBOutlet var ScanBlock: UIView!
    @IBOutlet var ShadowBlock: UIView!
    
    
    @IBAction func scanAction(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.microbeLM.frame.origin.x = -self.microbeLM.frame.width
            self.microbeRM.frame.origin.x = self.microbeRM.frame.origin.x + self.microbeRM.frame.width - 40
            self.bottomBar.frame.origin.y = self.bottomBar.frame.origin.y + self.bottomBar.frame.height
        }, completion:{ finished in
            if finished {
                self.scanAnimate(self.microbeLM, self.microbeRM, self.bottomBar, self.tappedIMG, self.microbeScanImg, self.microbeShareImg)
            }
        })
    }
    var i:Int = 1
    
    var tappedIMG:Int! = 0
    
    func scanAnimate(_ left: UIView, _ right: UIView, _ bot: UIView, _ id: Int, _ scan: UIImageView, _ share: UIImageView)
    {
        ShadowBlock.alpha = 1
        ScanBlock.alpha = 1
        UIView.animate(withDuration: 3.0, delay: 0, options: .curveLinear, animations: {
            self.ScanBlock.frame.origin.y = -30
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveLinear, animations: {
                        self.ShadowBlock.frame.origin.y = -self.ShadowBlock.frame.height
                }, completion:{ finished in
                    if finished {
                        self.ScanBlock.rotate(180)
                        UIView.animate(withDuration: 3.0, delay: 0.0, options: .curveLinear, animations: {
                            self.ScanBlock.frame.origin.y = self.view.frame.height - 15
                        }, completion:{ finished in
                            if finished {
                                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveLinear, animations: {
                                    self.ShadowBlock.frame.origin.y = -self.ShadowBlock.frame.height
                                }, completion:{ finished in
                                    if finished {
                                        self.ScanBlock.rotate(360)
                                        self.i = self.i + 1
                                        if (self.i == 2){
                                            self.i = 1
                                            self.ShadowBlock.alpha = 0
                                            self.ScanBlock.alpha = 0
                                            handCleanVC.showAnimate(left, right, bot, id, scan, share)
                                        } else {
                                            self.scanAnimate(left, right, bot, id, scan, share)
                                        }
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tutorial = defaults.bool(forKey: "tutorial")
        
        if tutorial {
//            defaults.removeObject(forKey: "tutorial")
            print (tutorial)
        } else {
//            defaults.set(true, forKey: "tutorial")
        }
        
        frameExtractor = FrameExtractor()
        frameExtractor.delegate = self
        
        ShadowBlock.alpha = 0
        ScanBlock.alpha = 0
        
        ScanBlock.applyGradient(colors: [UIColor(hexString: "#f88529", alpha: 0), UIColor(hexString: "#f88529"), UIColor.white, UIColor.white, UIColor(hexString: "#f88529"), UIColor(hexString: "#f88529", alpha: 0)], locations: [0, 0.45, 0.46, 0.54, 0.55, 1.2], angle: 180)
        
        ShadowBlock.applyGradient(colors: [UIColor(hexString: "#f88529", alpha: 0), UIColor(hexString: "#f88529", alpha: 0.7)], locations: [0, 1], angle: 180)
        
        addChildViewController(popOverVC)
        popOverVC.view.frame = view.frame
        popOverVC.view.frame.origin.x = -view.frame.width - 100
        view.addSubview(popOverVC.view)

        addChildViewController(handCleanVC)
        handCleanVC.view.frame = view.frame
        handCleanVC.view.frame.origin.x = -view.frame.width - 100
        view.addSubview(handCleanVC.view)

        addChildViewController(popOverVCRM)
        popOverVCRM.view.frame = view.frame
        popOverVCRM.view.frame.origin.x = view.frame.width
        view.addSubview(popOverVCRM.view)
        
        microbeOptionsList = [
            MicrobeOption(id: 1, picture: UIImage(named: "MIcrobe")!),
            MicrobeOption(id: 2, picture: UIImage(named: "twoMicrobe")!),
            MicrobeOption(id: 3, picture: UIImage(named: "threeMicrobe")!)
        ]
        
        microbeOptions.delegate = self
        microbeOptions.dataSource = self
        microbeOptions.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        microbeOptions.alpha = 0
        microbeOptions.layer.cornerRadius = microbeOptions.frame.width / 2
        microbeOptionPos.constant = -microbeOptions.frame.height
        microbeOptionsHeight.constant = CGFloat(microbeOptionsList.count * 64 + (microbeOptionsList.count - 1) * 10 + 95)

        bottomBarBackgroud.layer.cornerRadius = 40
        bottomBarBackgroud.layer.shadowColor = UIColor.gray.cgColor
        bottomBarBackgroud.layer.shadowOffset = .zero
        bottomBarBackgroud.layer.shadowRadius = 10
        bottomBarBackgroud.layer.shadowOpacity = 0.7
        
        microbeLM.backgroundColor = .white
        microbeLM.layer.cornerRadius = 20
        microbeLM.layer.shadowColor = UIColor.gray.cgColor
        microbeLM.layer.shadowOffset = .zero
        microbeLM.layer.shadowRadius = 10
        microbeLM.layer.shadowOpacity = 0.7

        microbeRM.backgroundColor = .white
        microbeRM.layer.cornerRadius = 20
        microbeRM.layer.shadowColor = UIColor.gray.cgColor
        microbeRM.layer.shadowOffset = .zero
        microbeRM.layer.shadowRadius = 10
        microbeRM.layer.shadowOpacity = 0.7
        
        tappedIMG = 1
        microbeBtn.layer.cornerRadius = 32
        microbeBtn.layer.shadowColor = UIColor(hexString: "#3f3f3f").cgColor
        microbeBtn.layer.shadowOffset = CGSize.zero
        microbeBtn.layer.shadowRadius = 40
        microbeBtn.layer.shadowOpacity = 0.3
        microbeBtn.applyGradient(colors: [UIColor(hexString: "#31b4c2"), UIColor(hexString: "#2ddadc")], locations: [0, 1], angle: -125, corner:31)
        microbeImg.frame.size = microbeBtn.frame.size
        microbeImg.contentMode = .center
        microbeImg.image = UIImage(named: "MIcrobe")
        microbeImg.layer.cornerRadius = 30.5
        microbeBtn.addSubview(microbeImg)
        
        microbeScan.applyGradient(colors: [UIColor(hexString: "#ffa650"), UIColor(hexString: "#f86d29")], locations: [0, 1], angle: -125, corner: (microbeScan.frame.width / 2) - 1)
        microbeScan.layer.cornerRadius = microbeScan.frame.width / 2
        microbeScan.layer.shadowColor = UIColor(hexString: "#3f3f3f").cgColor
        microbeScan.layer.shadowOffset = .zero
        microbeScan.layer.shadowRadius = 40
        microbeScan.layer.shadowOpacity = 0.3
        microbeScanImg.frame.size = microbeScan.frame.size
        microbeScanImg.contentMode = .center
        microbeScanImg.image = UIImage(named: "start scanning")
        microbeScanImg.layer.cornerRadius = 30.5
        microbeScan.addSubview(microbeScanImg)
        
        microbeShare.applyGradient(colors: [UIColor(hexString: "#31b4c2"), UIColor(hexString: "#2ddadc")], locations: [0, 1], angle: -125, corner:31)
        microbeShare.layer.cornerRadius = microbeShare.frame.width / 2
        microbeShare.layer.shadowColor = UIColor(hexString: "#3f3f3f").cgColor
        microbeShare.layer.shadowOffset = .zero
        microbeShare.layer.shadowRadius = 40
        microbeShare.layer.shadowOpacity = 0.3
        microbeShareImg.frame.size = microbeShare.frame.size
        microbeShareImg.contentMode = .center
        microbeShareImg.image = UIImage(named: "share")
        microbeShareImg.layer.cornerRadius = 30.5
        microbeShare.addSubview(microbeShareImg)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped))
        microbeCamera.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    @objc func imageTapped()
    {
        // just close
        DispatchQueue.main.async {
            if self.optionShown {
//                self.microbeImg.image = self.tappedIMG
//                self.microbeBtn.backgroundColor = self.tappedIMGcolor
                self.optionShown = !self.optionShown
                self.microbeOptionPos.constant = -self.microbeOptionsHeight.constant
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                    self.microbeOptions.alpha = 0
                    self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    
    @IBAction func share(_ sender: Any) {
        let textToShare = "Hey, look!"
        
        if let shareLinkURL = NSURL(string: "htts://vk.com") {
            let objectsToShare = [textToShare, shareLinkURL] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

            activityVC.excludedActivityTypes = [.addToReadingList,.assignToContact]
        
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}

extension FirstController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 64, height: 64)
    }
}

extension FirstController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.microbeOptionsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.microbeOptions.dequeueReusableCell(withReuseIdentifier: self.cellID, for: indexPath) as! MicrobeOptionCell
     
        let option = microbeOptionsList[indexPath.row]
        cell.optionID = option.id
        if !cell.setuped {
            cell.setupCell()
        }
        cell.optionPicture.image = option.picture

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.microbeOptions.cellForItem(at: indexPath) as! MicrobeOptionCell
        self.microbeImg.backgroundColor = .clear
        self.microbeImg.image = cell.optionPicture.image
        
        self.tappedIMG = cell.optionID
        
        self.optionShown = !self.optionShown
        self.microbeOptionPos.constant = -self.microbeOptionsHeight.constant
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.microbeOptions.alpha = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        // select dirty level
    }
}

class MicrobeOptionCell: UICollectionViewCell {
    
    var optionID : Int!
    
    @IBOutlet var optionPicture: UIImageView!
    
    var setuped = false
    func setupCell(){
        setuped = true
        self.applyGradient(colors: [UIColor(hexString: "#31b4c2"), UIColor(hexString: "#2ddadc")], locations: [0, 1], angle: -125, corner:31)
        self.layer.cornerRadius = 32
    }
}

class MicrobeOption: NSObject {
    
    var id: Int!
    var picture: UIImage!
    
    init(id:Int, picture:UIImage) {
        self.id = id
        self.picture = picture
    }
    
}
