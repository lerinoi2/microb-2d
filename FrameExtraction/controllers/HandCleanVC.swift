//
//  HandCleanVC.swift
//  FrameExtraction
//
//  Created by Ефремов Михаил on 13.07.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import UIKit

class HandCleanVC: UIViewController {

    @IBOutlet var succesBlock: UIView!
    @IBOutlet var darkFrame: UIImageView!
    @IBOutlet var icon: UIImageView!
    @IBOutlet var text: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        succesBlock.layer.shadowColor = UIColor.gray.cgColor
        succesBlock.layer.shadowOffset = .zero
        succesBlock.layer.shadowRadius = 10
        succesBlock.layer.shadowOpacity = 0.7
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        darkFrame.isUserInteractionEnabled = true
        darkFrame.addGestureRecognizer(tapGestureRecognizer)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        removeAnimate()
    }
    
    
    var lView : UIView!
    var rView : UIView!
    var bView : UIView!
    var clckBtn : Int!
    var scanBtn: UIImageView!
    var shareBtn: UIImageView!
    
    func showAnimate(_ left: UIView, _ right: UIView, _ bot: UIView, _ clcBtn: Int, _ scan: UIImageView, _ share: UIImageView)
    {
        if (clcBtn > 0){
            let gradient = self.succesBlock.layer.sublayers?.first(where: { (layer) -> Bool in
                return layer.name == "gradient"
            })
            gradient?.removeFromSuperlayer()
            self.text.text = "Microbes are detected!"
            self.icon.image = UIImage(named: "alert-icon")
            self.succesBlock.applyGradient(colors: [UIColor(hexString: "#f86d29"), UIColor(hexString: "#ffa650")], locations: [0, 1], angle: 125, corner:20)
        } else {
            let gradient = self.succesBlock.layer.sublayers?.first(where: { (layer) -> Bool in
                return layer.name == "gradient"
            })
            gradient?.removeFromSuperlayer()
            self.text.text = "Your hand is clean!"
            self.icon.image = UIImage(named: "tick-icon")
            self.succesBlock.applyGradient(colors: [UIColor(hexString: "#31b4c2"), UIColor(hexString: "#2ddadc")], locations: [0, 1], angle: 125, corner:20)
        }
        
        scan.image = UIImage(named: "continue")
        share.image = UIImage(named: "share-photo")
        
        lView = left
        rView = right
        bView = bot
        clckBtn = clcBtn
        scanBtn = scan
        shareBtn = share
         
        view.alpha = 1.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.frame.origin.x = 0
            self.bView.frame.origin.y = self.bView.frame.origin.y - self.bView.frame.height
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
                    if (clcBtn == 0){
                        self.darkFrame.image = UIImage(named: "stars")
                    }
                }, completion:{ finished in
                    if finished {
                        UIView.animate(withDuration: 0.3, delay: 3.0, options: .curveEaseOut, animations: {
                            self.darkFrame.alpha = 0
                        }, completion:{ finished in
                            if finished {
                                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
                                    self.view.frame.origin.x = -self.view.frame.width - 50
                                }, completion: nil)
                            }
                        })
                    }
                })
            }
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.darkFrame.alpha = 0
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.view.frame.origin.x = -self.view.frame.width - 50
//                    self.lView.frame.origin.x = -30
//                    self.rView.frame.origin.x = self.view.frame.width - 64
//                    self.bView.frame.origin.y = self.bView.frame.origin.y - self.bView.frame.height
                }, completion:{ finished in
                    if finished {
                        self.darkFrame.alpha = 1
                    }
                })
            }
        });
    }
}
