//
//  BannerVC.swift
//  FrameExtraction
//
//  Created by Ylee mini on 17.07.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import UIKit

class BannerVC: UIViewController {

    @IBOutlet var darkFrame: UIImageView!
    @IBOutlet var viewBanner: UIView!
    @IBOutlet var bannerPic: UIImageView!
    @IBOutlet var namePack: UILabel!
    @IBOutlet var backgroundPrice: UIView!
    @IBOutlet var priceText: UILabel!
    @IBOutlet var backgroundButton: UIView!
    @IBOutlet var buttonTry: UIButton!
    @IBOutlet var closeBtn: UIButton!
    
    @IBAction func close(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBanner.layer.cornerRadius = 20
        backgroundButton.applyGradient(colors: [UIColor(hexString: "#ffa650"), UIColor(hexString: "#f86d29")], locations: [0, 1], angle: -125, corner:18)
        print(backgroundButton.frame.width)
        backgroundPrice.applyGradient(colors: [UIColor(hexString: "#f86d29"), UIColor(hexString: "#ffa650")], locations: [0, 1], angle: -125, corner:30)
        backgroundPrice.layer.shadowColor = UIColor.gray.cgColor
        backgroundPrice.layer.shadowOffset = .zero
        backgroundPrice.layer.shadowRadius = 10
        backgroundPrice.layer.shadowOpacity = 0.5
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        darkFrame.isUserInteractionEnabled = true
        darkFrame.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        removeAnimate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func showAnimate()
    {
        view.alpha = 1.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.frame.origin.x = 0
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
                    self.darkFrame.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                }, completion: nil)
            }
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.darkFrame.backgroundColor = UIColor.black.withAlphaComponent(0)
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.view.frame.origin.x = -self.view.frame.width - 100
                }, completion:nil)
            }
        });
    }
    
}
