//
//  ContactUs.swift
//  FrameExtraction
//
//  Created by Ефремов Михаил on 12.07.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import UIKit

class ContactUs: UIViewController {

    @IBOutlet var textMessage: UITextField!
    @IBOutlet var buttonSubmit: UIView!
    @IBOutlet var themeText: UITextField!
    @IBOutlet var emailText: UITextField!
    
    @IBAction func closeButton(_ sender: Any) {
        removeAnimate()
    }
    
    @IBAction func submitAction(_ sender: Any) {
        sendMsgVC.showAnimate()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textMessage.layer.shadowColor = UIColor.gray.cgColor
        textMessage.layer.shadowOffset = .zero
        textMessage.layer.shadowRadius = 10
        textMessage.layer.shadowOpacity = 0.3
        
        themeText.layer.shadowColor = UIColor.gray.cgColor
        themeText.layer.shadowOffset = .zero
        themeText.layer.shadowRadius = 10
        themeText.layer.shadowOpacity = 0.3
        
        emailText.layer.shadowColor = UIColor.gray.cgColor
        emailText.layer.shadowOffset = .zero
        emailText.layer.shadowRadius = 10
        emailText.layer.shadowOpacity = 0.3
        
        buttonSubmit.layer.shadowColor = UIColor.gray.cgColor
        buttonSubmit.layer.shadowOffset = .zero
        buttonSubmit.layer.shadowRadius = 10
        buttonSubmit.layer.shadowOpacity = 0.3
        
        buttonSubmit.layer.cornerRadius = 20
        buttonSubmit.applyGradient(colors: [UIColor(hexString: "#f86d29"), UIColor(hexString: "#ffa650")], locations: [0, 1], angle: 125, corner:20)
        
        
        addChildViewController(sendMsgVC)
        sendMsgVC.view.frame = view.frame
        sendMsgVC.view.frame.origin.x = -view.frame.width - 100
        view.addSubview(sendMsgVC.view)
    }
    
    func showAnimate()
    {
        view.alpha = 1.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.frame.origin.x = 0
         }, completion: nil)
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.view.frame.origin.x = -self.view.frame.width
        }, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
