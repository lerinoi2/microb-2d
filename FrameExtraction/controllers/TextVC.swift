//
//  TextVC.swift
//  FrameExtraction
//
//  Created by Ефремов Михаил on 13.07.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import UIKit

class TextVC: UIViewController {
    
    
    @IBOutlet var text: UILabel!
    

    @IBAction func closeBtn(_ sender: Any) {
        removeAnimate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    var txt : String!
    
    func showAnimate(_ txt: String)
    {
        text.text = txt
        view.alpha = 1.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.frame.origin.x = 0
        }, completion: nil)
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.view.frame.origin.x = self.view.frame.width
        }, completion: nil)
    }

}
