//
//  PopUpViewController.swift
//  FrameExtraction
//
//  Created by Ефремов Михаил on 10.07.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import UIKit

class PopUpViewController: UIViewController {
    
    @IBOutlet var microbeOptionsP: UICollectionView!
    
    var microbeOptionsListP: [MicrobeOptionP] = []
    
    let cellID = "microbeOptionIDP"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        microbeOptionsListP = [
            MicrobeOptionP(id: 1, picture: UIImage(named: "white-arrow")!, text: "Choose Microbe pack"),
            MicrobeOptionP(id: 5, picture: UIImage(named: "arrow2")!, text: "Contact us"),
        ]
        microbeOptionsP.layer.cornerRadius = 30
        microbeOptionsP.delegate = self
        microbeOptionsP.dataSource = self
        
        addChildViewController(contactUs)
        contactUs.view.frame = view.frame
        contactUs.view.frame.origin.x = -view.frame.width
        view.addSubview(contactUs.view)
        
        addChildViewController(shopVC)
        shopVC.view.frame = view.frame
        shopVC.view.frame.origin.x = -view.frame.width
        view.addSubview(shopVC.view)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        darkFrame.isUserInteractionEnabled = true
        darkFrame.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        removeAnimate()
    }

    @IBOutlet var darkFrame: UIImageView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closePopUp(_ sender: Any) {
        self.removeAnimate()
    }
    
    var showBtn : UIView!
    
    func showAnimate(_ btn: UIView)
    {
        showBtn = btn
        view.alpha = 1.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.frame.origin.x = 0
            self.showBtn.frame.origin.x = self.view.frame.width - 80
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
                    self.darkFrame.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                }, completion: nil)
            }
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.darkFrame.backgroundColor = UIColor.black.withAlphaComponent(0)
        }, completion:{ finished in
            if finished {
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.view.frame.origin.x = -self.view.frame.width - 50
                    self.showBtn.frame.origin.x = -40
                }, completion:nil)
            }
        });
    }
}

extension PopUpViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (indexPath == [0, 0]){
           return CGSize(width: self.view.frame.width + 50, height: 150)
        }
        else {
            return CGSize(width: self.view.frame.width + 50, height: 64)
        }
    }
}

extension PopUpViewController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.microbeOptionsListP.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.microbeOptionsP.dequeueReusableCell(withReuseIdentifier: self.cellID, for: indexPath) as! MicrobeOptionCellP
        
        let option = microbeOptionsListP[indexPath.row]
        cell.optionID = option.id
        if !cell.setuped {
            cell.setupCell()
        }
        
        if (option.id == 5){
            cell.packPic.alpha = 0
        } else {
            cell.optionTextP.alpha = 0
        }
        
        cell.optionPictureP.image = option.picture
        cell.optionTextP.text = option.text
        
        return cell
    }
}

class MicrobeOptionCellP: UICollectionViewCell{
    var optionID: Int!
    
    @IBOutlet var optionPictureP: UIImageView!
    @IBOutlet var optionTextP: UILabel!
    @IBOutlet var packPic: UIImageView!
    
    let border = CALayer()
    let width = CGFloat(1.0)
    

    var setuped = false
    func setupCell(){
        if (optionID == 1){
            self.optionTextP.textColor = .white
            self.applyGradient(colors: [UIColor(hexString: "#31b4c2"), UIColor(hexString: "#2ddadc")], locations: [0, 1], angle: -125)
        }
        setuped = true
    }
    
    override var isSelected: Bool{
        didSet{
            if self.isSelected
            {
                if (optionID == 1){
                    shopVC.showAnimate()
                }
                if (optionID == 5){
                    contactUs.showAnimate()
                }
            }
        }
    }
}

class MicrobeOptionP: NSObject {
    
    var id: Int!
    var picture: UIImage!
    var text: String
    
    init(id:Int, picture:UIImage, text:String) {
        self.id = id
        self.picture = picture
        self.text = text
    }
}

