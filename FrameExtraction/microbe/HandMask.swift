//
//  HandMask.swift
//  FrameExtraction
//
//  Created by Кладов Владимир on 19.06.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import Foundation
import UIKit

class HandMask{
    var contour: [CGPoint]!
    var size: CGSize!
    var path: CGMutablePath = CGMutablePath()
    
    init(contour: [CGPoint], size: CGSize){
        self.contour = contour
        self.size = size
        
        self.initPath()
    }
    
    func initPath(){
        
        var first = true
        for point in contour {
            if first {
                path.move(to: point)
                first = false
            } else {
                path.addLine(to: point)
            }
        }
        path.closeSubpath()
        //path.addEllipse(in: CGRect(x: self.size.width/2, y: self.size.height/2, width: self.size.width/2, height: self.size.height/2))
    }
    
    public func getPicture() -> UIImage{
        UIGraphicsBeginImageContext(self.size)
        let ctx = UIGraphicsGetCurrentContext()!
        
        
        ctx.setFillColor(UIColor.red.cgColor)
        ctx.setStrokeColor(UIColor.green.cgColor)
        ctx.setLineWidth(10)
        let rectangle = CGRect(x: 0, y: 0, width: 512, height: 512)
        ctx.addRect(rectangle)
        ctx.drawPath(using: .fillStroke)
        
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
