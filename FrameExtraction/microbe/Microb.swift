//
//  Microb.swift
//  FrameExtraction
//
//  Created by Кладов Владимир on 15.06.2018.
//  Copyright © 2018 bRo. All rights reserved.
//

import UIKit
import Foundation

class Microb {
    var A: CGPoint!,
        B: CGPoint!,
        picture: UIImage!,
        backPicture: UIImage!,
        A1 = CGPoint(x: 0, y: 0),
        size_rotated: CGSize = CGSize(width: 0, height: 0),
        A_angle: CGFloat = 0,
        A_r: CGFloat = 0,
        new_angle: CGFloat = 0,
        clipMask: CGMutablePath!,
        prevA1 = CGPoint(x: -1, y: 0),
        prevB1 = CGPoint(x: -1, y: 0)
    
    init(picture: UIImage, A: CGPoint, B: CGPoint){
        self.picture = picture
        self.A = A
        self.B = B
        self.calcPointAAngle()
    }
    
    init(picture: UIImage, backPicture: UIImage, A: CGPoint, B: CGPoint, clipMask: CGMutablePath){
        self.picture = picture
        self.A = A
        self.B = B
        self.calcPointAAngle()
        self.clipMask = clipMask
    }
    func clipImage(size: CGSize) -> UIImage{
        
        UIGraphicsBeginImageContext(size)
        let ctx = UIGraphicsGetCurrentContext()!
        ctx.translateBy(x: 0, y: size.height)
        ctx.scaleBy(x: 1, y: -1)
        
        ctx.setFillColor(UIColor.red.cgColor)
        //ctx.addPath(self.clipMask)
        ctx.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        ctx.setBlendMode(.clear)
        ctx.setFillColor(UIColor.clear.cgColor)
        ctx.addPath(self.clipMask)
        ctx.fillPath()
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    public func getClipedImage(image: UIImage) -> UIImage{
        let rect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        
        UIGraphicsBeginImageContext(image.size)
        let ctx = UIGraphicsGetCurrentContext()!
        ctx.translateBy(x: 0, y: image.size.height)
        ctx.scaleBy(x: 1, y: -1)
        
        ctx.clip(to: rect, mask: self.clipImage(size: image.size).cgImage!)
        ctx.draw(image.cgImage!, in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    public func getMeWhatINeed(size: CGSize, A1: CGPoint, B1: CGPoint) -> UIImage{
        if(self.backPicture != nil){
            return self.getRotatedScaledAndTranslatedImage(size: size, A1: A1, B1: B1)
        }else{
            return self.getRotatedScaledAndTranslatedImage(size: size, A1: A1, B1: B1)
        }
    }
    public func getRotatedScaledAndTranslatedImage(size: CGSize, A1: CGPoint, B1: CGPoint, checkPrev: Bool = false) -> UIImage{
        var A_1 = A1,
            B_1 = B1
        if(checkPrev && self.prevA1.x > 0 && self.prevB1.x > 0){
            let dx_a = A1.x - prevA1.x,
                dy_a = A1.y - prevA1.y,
                dx_b = B1.x - prevB1.x,
                dy_b = B1.y - prevB1.y
            
            
            let delta: Float = 3,
                del: CGFloat = 3
            
            if(fabsf(Float(dx_a)) > delta){
                A_1.x = prevA1.x + dx_a/del
            }
            if(fabsf(Float(dy_a)) > delta){
                A_1.y = prevA1.y + dy_a/del
            }
            if(fabsf(Float(dx_b)) > delta){
                B_1.x = prevB1.x + dx_b/del
            }
            if(fabsf(Float(dy_b)) > delta){
                B_1.y = prevB1.y + dy_b/del
            }
        }
        
        self.prevA1 = A_1
        self.prevB1 = B_1
        
        let ba = CGVector(dx: A.x - B.x, dy: A.y - B.y)
        let bc = CGVector(dx: A_1.x - B_1.x, dy: A_1.y - B_1.y)
        self.new_angle = self.rotateAngleFromFirstToSecondVectors(ba: ba, bc: bc)
        let scale: Float = Float(bc.length()/ba.length())
        
        let image = self.picture.imageRotatedByDegrees(degrees: self.new_angle)
        
        self.size_rotated = image.size
        self.calcA1Position()
        
        let scaledImage = image.scaledImage(scale: scale)
        
        UIGraphicsBeginImageContext(size)
        let bitmap = UIGraphicsGetCurrentContext()!
        bitmap.translateBy(x: 0, y: size.height)
        bitmap.scaleBy(x: 1, y: -1)
        
        
//        if(self.clipMask != nil){
//            bitmap.clip(to: CGRect(x: 0, y: 0, width: size.width, height: size.height), mask: self.clipImage(size: size).cgImage!)
//        }
        
        
        
        let rect = CGRect(x: A_1.x - self.A1.x*CGFloat(scale), y: A_1.y - self.A1.y*CGFloat(scale), width: scaledImage.size.width, height: scaledImage.size.height)
        bitmap.draw(scaledImage.cgImage!, in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        
        return newImage
    }
    
    func rotateAngleFromFirstToSecondVectors(ba: CGVector, bc: CGVector) -> CGFloat {
        let ba_m = sqrt(ba.dx*ba.dx + ba.dy*ba.dy),
            bc_m = sqrt(bc.dx*bc.dx + bc.dy*bc.dy),
            cosBA = ba.dx/ba_m,
            sinBA = ba.dy/ba_m,
            cosBC = bc.dx/bc_m,
            sinBC = bc.dy/bc_m,
            degBA = acosf(Float(cosBA)) * (sinBA < 0 ? -1 : 1),
            degBC = acosf(Float(cosBC)) * (sinBC < 0 ? -1 : 1);
    
        return CGFloat(degBA - degBC);
    }
    func calcPointAAngle(){
        let r = sqrt(pow((self.picture.size.width/2 - A.x), 2) + pow((self.picture.size.height/2 - self.A.y), 2)),
        cos = (self.A.x - self.picture.size.width/2)/r,
        sin = (self.picture.size.height/2 - self.A.y)/r,
        Angle = acosf(Float(cos)) * (sin < 0 ? -1 : 1);
    
        A_r = r;
        A_angle = CGFloat(Angle);
    }

    
    func calcA1Position(){
        A1 = CGPoint(
            x: size_rotated.width/2 + self.A_r * CGFloat(cosf(Float(self.new_angle) + Float(self.A_angle))),
            y: size_rotated.height - (size_rotated.height/2 + self.A_r * CGFloat(sinf(Float(self.new_angle) + Float(self.A_angle))))
        )
    };

}

extension CGVector {
    public func length() -> CGFloat {
        return sqrt(pow(self.dx, 2) + pow(self.dy, 2))
    }
}

extension UIImage {
    public func imageRotatedByDegrees(degrees: CGFloat) -> UIImage {
        
        // calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox = UIView(frame: CGRect(origin: CGPoint.zero, size: size))
        let t = CGAffineTransform(rotationAngle: degrees);
        
        rotatedViewBox.transform = t
        let rotatedSize = rotatedViewBox.frame.size
        
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap = UIGraphicsGetCurrentContext()!
        
//        bitmap.setStrokeColor(UIColor.green.cgColor)
//        bitmap.setLineWidth(1.0)
//        bitmap.addRect(CGRect(x: 0, y: 0, width: rotatedSize.width, height: rotatedSize.height))
//        bitmap.drawPath(using: .stroke)
        
        
        
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2.0, y: rotatedSize.height / 2.0)
        //   // Rotate the image context
        bitmap.rotate(by: degrees)
        
        // Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        
        let rect = CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height)
        bitmap.draw(self.cgImage!, in: rect)
        
//        bitmap.setStrokeColor(UIColor.red.cgColor)
//        bitmap.setLineWidth(1.0)
//        bitmap.addRect(rect)
//        bitmap.drawPath(using: .stroke)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
    func scaledImage(scale: Float) -> UIImage {
        let new_size = CGSize(width: self.size.width*CGFloat(scale), height:  self.size.height*CGFloat(scale))
        UIGraphicsBeginImageContext(new_size)
        let bitmap = UIGraphicsGetCurrentContext()!
        
        bitmap.translateBy(x: 0, y: new_size.height)
        bitmap.scaleBy(x: 1, y: -1)
        let rect = CGRect(x: 0, y: 0, width: new_size.width, height: new_size.height)
        bitmap.draw(self.cgImage!, in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
    func fixOrientation() -> UIImage {
        
        guard let cgImage = cgImage else { return self }
        
        if imageOrientation == .up { return self }
        
        var transform = CGAffineTransform.identity
        
        switch imageOrientation {
            
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(Float.pi))
            
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Float.pi))
            
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat(-Float.pi))
            
        case .up, .upMirrored:
            break
        }
        
        switch imageOrientation {
            
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
            
        case .up, .down, .left, .right:
            break
        }
        
        if let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: cgImage.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) {
            
            ctx.concatenate(transform)
            
            switch imageOrientation {
                
            case .left, .leftMirrored, .right, .rightMirrored:
                ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
                
            default:
                ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            }
            
            if let finalImage = ctx.makeImage() {
                return (UIImage(cgImage: finalImage))
            }
        }
        
        // something failed -- return original
        return self
    }
}
