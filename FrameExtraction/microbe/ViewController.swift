//
//  ViewController.swift
//  Created by Bobo on 29/12/2016.
//

import UIKit

class ViewController: UIViewController, FrameExtractorDelegate {
   
    var frameExtractor: FrameExtractor!
    var step = 0
    var sliderValue = 0.0
    var type = 0
    var prevFrameWasGood = false
    var pickerData: [String] = []
    var selectedValueIndex = 0
    var needGetContour = false
    var needCapture = false
    var needPhoto = false
    var contour = [Int]()
    var keyPoints = [Int]()
    var points = [String: Int]()
    var center = CGPoint(x: 0.0, y: 0.0)
    var cont = [CGPoint]()
    var startingImage: UIImage!
    var myPoint = CGPoint(x: 0, y: 0)
    var angle: Float = 0
    let microbs: [Int: Microb] = [
//        0: Microb(picture: UIImage(named: "m1")!, A: CGPoint(x: 29, y: 66), B: CGPoint(x: 55, y: 66)),
        0: Microb(picture: UIImage(named: "m1")!, A: CGPoint(x: 30, y: 10), B: CGPoint(x: 55, y: 70)),
        1: Microb(picture: UIImage(named: "m22")!, A: CGPoint(x: 23, y: 71), B: CGPoint(x: 57, y: 71)),
        2: Microb(picture: UIImage(named: "m28")!, A: CGPoint(x: 26, y: 84), B: CGPoint(x: 52, y: 84)),
        3: Microb(picture: UIImage(named: "m24")!, A: CGPoint(x: 28, y: 64), B: CGPoint(x: 59, y: 64)),
        4: Microb(picture: UIImage(named: "m25")!, A: CGPoint(x: 20, y: 81), B: CGPoint(x: 63, y: 81)),
//        5: Microb(picture: UIImage(named: "m26")!, A: CGPoint(x: 35, y: 67), B: CGPoint(x: 48, y: 67))
        5: Microb(picture: UIImage(named: "m6")!, A: CGPoint(x: 20, y: 67), B: CGPoint(x: 17, y: 86))
    ];

    @IBAction func xSliderAction(_ sender: UISlider) {
        self.myPoint.x = CGFloat(sender.value)
        self.updateImage()
        self.updateLabel()
        
    }
    @IBAction func ySliderAction(_ sender: UISlider) {
        self.myPoint.y = CGFloat(sender.value)
        self.updateImage()
        self.updateLabel()
    }
    @IBAction func rotateSliderAction(_ sender: UISlider) {
        self.angle = sender.value
        self.updateImage()
        self.updateLabel()
    }
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var debugLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBAction func continueButtonAction(_ sender: UIButton) {
        self.needCapture = false
    }
    
    @IBAction func flipButton(_ sender: UIButton) {
        frameExtractor.flipCamera()
    }
    @IBAction func touchButtonAction(_ sender: UIButton) {
        needGetContour = true
    }
    
    
    @IBOutlet weak var button: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setupGestures()
        frameExtractor = FrameExtractor()
        frameExtractor.delegate = self
        
        if((UserDefaults.standard.object(forKey: "contour")) != nil){
            contour = UserDefaults.standard.object(forKey: "contour") as! [Int]
            keyPoints = OpenCVWrapper.tempContourKeyPoints(contour) as! [Int]
        }
    }
    
//    private func setupGestures(){
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapped))
//        tapGesture.numberOfTapsRequired = 1
//        button.addGestureRecognizer(tapGesture)
//    }
    
//    @objc
//    private func tapped() {
//        guard let popVC = storyboard?.instantiateViewController(withIdentifier: "popVC") else { return }
//
//        popVC.modalPresentationStyle = .popover
//
//        let popOverVC = popVC.popoverPresentationController
////        popOverVC?.delegate = self
////        popOverVC?.sourceView = self.button
////        popOverVC?.sourceRect = CGRect(x: self.button.bounds.midX, y: self.button.bounds.minY, width: 0, height: 0)
////        popVC.preferredContentSize = CGSize(width: 250, height: 250)
//
//        self.present(popVC, animated: true)
//    }
    
    
    func updateImage(){
        DispatchQueue.main.async {
            self.imageView.image = self.DrawOnImage(startingImage: self.startingImage)
        }
    }
    
    func updateLabel(){
        DispatchQueue.main.async {
            self.debugLabel.text = "x: \(Int(self.myPoint.x)), y: \(Int(self.myPoint.y)), A: \(Int(self.angle))"
        }
    }
    
    func getVectorByDegree(degree: Float, dist: Float) -> CGVector{
        let rad = degree * Float.pi / 180
        return CGVector(dx: Int(dist*cos(rad)), dy: Int(dist*sin(rad)))
    }
    
    func pathBetween(indexA: Int, indexB: Int) -> [CGPoint]{
        var start = indexA
        func getNext(index: Int, size: Int) -> Int {
            return index == size - 1 ? 0 : index + 1
        }
        func getPrev(index: Int, size: Int) -> Int {
            return index == 0 ? size - 1 : index - 1
        }
        
        print("---")
        print(indexB - indexA)
        print(self.cont.count - indexB + indexA)
        
        
        let next = (indexB - indexA) < 0 ? getPrev : (indexB - indexA < self.cont.count - indexB - indexA ? getNext : getPrev)
        
        var arPoints: [CGPoint] = [self.cont[indexA]]
        var nextIndex = next(start, self.cont.count)
        while nextIndex != indexB {
            arPoints.append(self.cont[nextIndex])
            nextIndex = next(nextIndex, self.cont.count)
        }
        arPoints.append(self.cont[indexB])
        
        return arPoints
    }
    
    func captured(image: UIImage) {
//        if(self.needGetContour){
//            self.contour = OpenCVWrapper.getContoursFrom(image) as! [Int]
//            self.needGetContour = false
//        }
        
//        if(self.needPhoto){
//            DispatchQueue.main.async {
//                self.imageView.image = self.DrawOnImage(startingImage: image)
//            }
//            self.needPhoto = false
//        }
        
        if(!self.needCapture){
        
        /*
            let img = OpenCVWrapper.showMeWhatYouGot(image)!
            DispatchQueue.main.async {
                self.imageView.image = img
            }
         */
            let currentPoints = OpenCVWrapper.getFingersPositionNew(image) as! [Int]
            if(currentPoints.count > 0){
                self.startingImage = image
                self.parseResult(points: currentPoints)
                self.needCapture = true
                self.needPhoto = true
            }
//            else{
//                DispatchQueue.main.async {
//                    self.imageView.image = image
//                    self.prevFrameWasGood = false
//                }
//            }
        
            DispatchQueue.main.async {
                self.imageView.image = self.DrawOnImage(startingImage: image)
                //self.prevFrameWasGood = true
            }
        }
    }
    
    func distBetween(x1: Int, y1: Int, x2: Int, y2: Int) -> Float{
        return sqrt(pow((Float(x1 - x2)), 2) + pow(Float(y1 - y2), 2))
    }
    
    func parseResult(points: [Int]){
        self.center = CGPoint(x: points[0], y: points[1])
        self.points["F1"] = points[2]
        self.points["F2"] = points[3]
        self.points["F3"] = points[4]
        self.points["F4"] = points[5]
        self.points["F5"] = points[6]
        
        self.points["d0"] = points[7]
        self.points["d12"] = points[8]
        self.points["d23"] = points[9]
        self.points["d34"] = points[10]
        self.points["d45"] = points[11]
        self.points["d5"] = points[12]
        
        cont.removeAll()
        var i: Int = 13
        while i < points.count{
            cont.append(CGPoint(x: points[i], y: points[i + 1]))
            i += 2
        }
    }
    
    func processCompare(points: [Int]){
        if(points.count > 6 && self.keyPoints.count > 1){
            let centerD = distBetween(x1: self.keyPoints[0], y1: self.keyPoints[1], x2: points[0], y2: points[1])
            
            var i = 2
            var max: Float = 0
            var x0: Int, y0: Int, x1: Int, y1: Int, summ: Float = 0
            while i < self.keyPoints.count {
                var j = 2
                var min: Float = -1
                var dist: Float = 0
                x0 = self.keyPoints[i]
                y0 = self.keyPoints[i+1]
                while j < points.count {
                    x1 = points[j]
                    y1 = points[j+1]
                    j += 2
                    
                    dist = distBetween(x1: x0, y1: y0, x2: x1, y2: y1)
                    if(min < 0 || dist < min){
                        min = dist
                    }
                }
                i += 2
                
                if(min > max){
                    max = min
                }
                
                summ += min
            }
            
            if(max < 20 && centerD < 20){
                self.needPhoto = true
                self.needCapture = true
            }
            
            DispatchQueue.main.async {
                self.debugLabel.text = "D: \(centerD) Summ: \(summ) Max: \(max)"
            }
        }
    }
    
    func pointOffset(_ index: Int) -> Float {
        var sum: Float = 0
        func length(_ i: Int) -> Float {
            let a = self.cont[i]
            let b = self.cont[i+1]
            
            return Float(sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2)))
        }
        
        var i = 0
        while i < index {
            sum += length(i)
            i += 1
        }
        
        return sum
    }
    
    func offsetBetween(a: Int, b: Int, proportion: Float = 0.5, min: Bool = true) -> Float{
        let offsetA = self.pointOffset(a)
        let offsetB = self.pointOffset(b)
        let contourLength = self.pointOffset(self.cont.count - 1)
        
        let first = fabsf(offsetA - offsetB)
        let second = offsetB > offsetA ? offsetA + (contourLength - offsetB) : offsetB + (contourLength - offsetA)
        
        var res: Float = 0
        if(first < second){
            res = offsetA < offsetB ? offsetA + (offsetB - offsetA)*proportion : offsetB + (offsetA - offsetB)*proportion
        }else{
            let d = second*proportion
            if(offsetA < offsetB){
                res = offsetA - d
                
                if(res < 0){
                    res = contourLength + res
                }
            }else{
                res = offsetA + d
                if(d > contourLength){
                    res = d - contourLength
                }
            }
        }
        
        return res;
    }
    
    func lineOnContour(offset: Float, dist: Float) -> [CGPoint] {
        var sum: Float = 0
        var sumNext: Float = 0
        var res: [CGPoint] = []
        func next(_ i: Int) -> Int { return i == self.cont.count - 1 ? 0 : i+1}
        func length(_ i: Int) -> Float {
            let a = self.cont[i]
            let b = self.cont[next(i)]
            
            return Float(sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2)))
        }
        func vectorAngle(vector: CGVector) -> Float{
            let r = Float(sqrt(pow(vector.dx, 2) + pow(vector.dy, 2)))
            let cos = Float(vector.dx)/r
            let sin = Float(vector.dy)/r
            return acosf(cos) * (sin < 0 ? -1 : 1)
        }
        func pointOnLine(start: CGPoint, end: CGPoint, length: Float) -> CGPoint {
            let rad = vectorAngle(vector: CGVector(dx: end.x - start.x, dy: end.y - start.y))
            let dx = CGFloat(length*cos(rad))
            let dy = CGFloat(length*sin(rad))
            
            return CGPoint(x: start.x + dx, y: start.y + dy)
        }
        
        var i = 0
        while sumNext < offset {
            sumNext = sum + length(i)
            if sumNext < offset {
                sum = sumNext
                i = next(i)
            }
        }
        var delta = offset - sum
        res.append(pointOnLine(start: self.cont[i], end: self.cont[next(i)], length: delta))
        
        
        while sumNext < offset + dist {
            sumNext = sum + length(i)
            if sumNext < offset + dist {
                sum = sumNext
                i = next(i)
            }
        }
        delta = (offset + dist) - sum
        res.append(pointOnLine(start: self.cont[i], end: self.cont[next(i)], length: delta))
        
        return res
    }
    
    func DrawOnImage(startingImage: UIImage) -> UIImage {
        
        guard let d23 = self.points["d23"] else { return startingImage }
        let d23Point = self.cont[d23]
//        let F1 = self.cont[self.points["F1"]!]
        let F2 = self.cont[self.points["F2"]!]
//        let F3 = self.cont[self.points["F3"]!]
//        let F4 = self.cont[self.points["F4"]!]
        let d34 = self.cont[self.points["d34"]!]
        
//        let clipMask = HandMask(contour: self.cont, size: startingImage.size).path
        
        var microbsPictures: [UIImage] = []
        
        
        
        let path = self.lineOnContour(offset: offsetBetween(a: self.points["d23"]!, b: self.points["F3"]!) - 25.0 + Float(self.myPoint.x), dist: 25.0)
        
        let pathF4d45 = self.lineOnContour(offset: offsetBetween(a: self.points["F4"]!, b: self.points["d45"]!)  + Float(self.myPoint.y), dist: 15.0)
        let pathF5d5 = self.lineOnContour(offset: offsetBetween(a: self.points["F5"]!, b: self.points["d5"]!, proportion: 0.1)  + Float(self.myPoint.y), dist: 15.0)
        
        microbsPictures.append(self.microbs[5]!.getRotatedScaledAndTranslatedImage(size: startingImage.size, A1: pathF4d45[1], B1: pathF4d45[0], checkPrev: self.prevFrameWasGood))
        microbsPictures.append(self.microbs[3]!.getRotatedScaledAndTranslatedImage(size: startingImage.size, A1: path[0], B1: path[1], checkPrev: self.prevFrameWasGood))
        microbsPictures.append(self.microbs[4]!.getRotatedScaledAndTranslatedImage(size: startingImage.size, A1: pathF5d5[1], B1: pathF5d5[0], checkPrev: self.prevFrameWasGood))
        
        microbsPictures.append(self.microbs[0]!.getRotatedScaledAndTranslatedImage(size: startingImage.size, A1: d23Point, B1: self.pointBetween(a: d23Point, b: center), checkPrev: self.prevFrameWasGood))
        
        microbsPictures.append(self.microbs[2]!.getRotatedScaledAndTranslatedImage(size: startingImage.size, A1: d34, B1: CGPoint(x: d34.x, y: d34.y + 4), checkPrev: self.prevFrameWasGood))
        microbsPictures.append(self.microbs[1]!.getRotatedScaledAndTranslatedImage(size: startingImage.size, A1: F2, B1: CGPoint(x: F2.x, y: F2.y + 10), checkPrev: self.prevFrameWasGood))
        
        
        UIGraphicsBeginImageContext(startingImage.size)
        
        // Draw the starting image in the current context as background
        startingImage.draw(at: CGPoint(x: 0, y: 0))
        
        // Get the current context
        let context = UIGraphicsGetCurrentContext()!
        
        
        
        for image in microbsPictures {
            context.draw(image.cgImage!, in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        }
        
        //let maskImage = HandMask(contour: self.cont, size: startingImage.size).getPicture()
        //context.draw(maskImage.cgImage!, in: CGRect(x: 0, y: 0, width: startingImage.size.width, height: startingImage.size.height))
        
        
        
        //Draw path points
//        for p in path{
//            context.setStrokeColor(UIColor.blue.cgColor)
//            context.setLineWidth(1.0)
//            context.addEllipse(in: CGRect(x: p.x, y: p.y, width: 2, height: 2))
//            context.drawPath(using: .stroke)
//        }
        
        // Save the context as a new UIImage
        let myImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // Return modified image
        return myImage!
    }
    func pointBetween(a: CGPoint, b: CGPoint) -> CGPoint {
        return CGPoint(x: a.x + (b.x - a.x)/2, y: a.y + (b.y - a.y)/2)
    }
    
    func angleBetween(a: CGPoint, b: CGPoint, c: CGPoint) -> CGFloat
    {
        let ba = CGVector(dx: (a.x - b.x), dy: -(a.y - b.y))
        let bc = CGVector(dx: (c.x - b.x), dy: -(c.y - b.y))
        
        // length square of both vectors
        let ba_m = sqrt(ba.dx * ba.dx + ba.dy * ba.dy)
        let bc_m = sqrt(bc.dx * bc.dx + bc.dy * bc.dy)
        
        // of cosine of the needed angle
        let cosBA = ba.dx/ba_m;
        let sinBA = ba.dy/ba_m;
        let cosBC = bc.dx/bc_m;
        let sinBC = bc.dy/bc_m;
        
        let degBA = acosf(Float(cosBA)) * (sinBA < 0 ? -1 : 1);
        let degBC = acosf(Float(cosBC)) * (sinBC < 0 ? -1 : 1);
        let delta = degBA - degBC;
        
        let rs = delta * 180.0 / Float.pi;
        
        return CGFloat(floor(rs + 0.5))
    }
}

//extension ViewController: UIPopoverPresentationControllerDelegate {
//    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
//        return .none
//    }
//}

